<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sehat Main Bola | @yield('title')</title>

    <!-- Scripts -->

    <script src="{{ asset('js/jquery.min.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.js') }}" defer></script>

    <script src="{{ asset('js/slick.js') }}" defer></script>
    <script src="{{ asset('js/waypoints.js') }}" defer></script>
    <script src="{{ asset('js/jquery.counterup.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}" defer></script>
    <script src="{{ asset('js/jquery.mixitup.js') }}" defer></script>
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}" defer></script>
    <script src="{{ asset('js/custom.js') }}" defer></script>


    <!-- Styles -->
    <!-- Font awesome -->
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/query.fancybox.css') }}" rel="stylesheet">
    <link id="switcher" href="{{ asset('css/theme-color/default-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <style type="text/css">
      html, body {
          max-width: 100%;
          overflow-x: hidden;
      }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135930930-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-135930930-1');
    </script>



</head>
<body>  

  <!--START SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>
      <span>Top</span>
    </a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Start header section -->
  <header id="mu-header">  
    <nav class="navbar navbar-inverse" style="border-radius:0px;">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" style="color:white;" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
              Menu <span class="caret"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="{{ asset('img/smb_logo.png')}}" alt="sehat main bola logo"></a>
          
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            @guest
                <li>
                    <a class="nav-link" href="{!! route('event_upcoming')!!}"><i class="fa fa-futbol-o" aria-hidden="true"></i> Jadwal Main Bareng</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> {{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                  <li>
                      <a class="nav-link" href="{{ route('register') }}"><i class="fa fa-pencil" aria-hidden="true"></i> {{ __('Register') }}</a>
                  </li>
                  <li>
                      <a lass="nav-link" href="https://wa.me/+6281218824244"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a>
                  </li>
                  <li>
                      <a class="nav-link" href="//instagram.com/sehatmainbola"><i class="fa fa-instagram" aria-hidden="true"></i> Gallery</a>
                  </li>
                  <li>
                      <a class="nav-link" href="{!! route('frontend_faq')!!}"><i class="fa fa-question" aria-hidden="true"></i> FAQ</a>
                  </li>
                  <li>
                      <a class="nav-link" href="{!! route('frontend_rules_point')!!}"><i class="fa fa-exchange" aria-hidden="true"></i> Tukar Poin Hadiah</a>
                  </li>
                  
                @endif
            @else
              <li class="nav-item" style="margin-top:15px;">
                <span id="pointLandingFrontEnd" class="badge badge-danger">{!! $findPoint!!} Poin</span>
              </li>
              <li >
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->name }} <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="{!! route('event_upcoming')!!}"><i class="fa fa-futbol-o" aria-hidden="true"></i> Jadwal Main Bareng</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{!! route('invoice_index')!!}"><i class="fa fa-file-text" aria-hidden="true"></i> History Join <span id="requestBadgeFrontend" class="badge badge-pill badge-danger">{!! $countRequested!!} Req</span></a> </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{!! route('point_index')!!}"><i class="fa fa-gift" aria-hidden="true"></i> History Point </a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{!! route('user_updateself')!!}"><i class="fa fa-user" aria-hidden="true"></i> My Profile </a></li>
                    <li role="separator" class="divider"></li>
                    <li role="separator" class="divider"></li>
                    <li role="separator" class="divider"></li>
                    <li>
                      <a lass="nav-link" href="https://wa.me/+6281218824244"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="nav-link" href="//instagram.com/sehatmainbola"><i class="fa fa-instagram" aria-hidden="true"></i> Gallery</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="nav-link" href="{!! route('frontend_faq')!!}"><i class="fa fa-question" aria-hidden="true"></i> FAQ</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                    <li>
                        <a class="nav-link" href="{!! route('frontend_rules_point')!!}"><i class="fa fa-exchange" aria-hidden="true"></i> Tukar Poin Hadiah</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                    <li>
                      <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('Logout') }}
                      </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>
                    </li>
                  </ul>
                </li>
                
              </li>
            @endguest
            
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    <!--<nav class="navbar navbar-default mu-main-navbar" role="navigation">  
      <div class="container">
        <div class="navbar-header">
 
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                                                            
          <a class="navbar-brand" href="/"><img src="asset('img/logo.png') }}" alt="Sehat Main Bola Logo"></a> 
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav navbar-right mu-main-nav">
             @guest
                <li>
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                  <li>
                      <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                  </li>
                @endif
            @else
              <li >
                <a href="/invoice" style="color:white">{{ Auth::user()->name }} <span class="fa fa-gear"></span></a>
              </li>
            @endguest
          </ul>                            
        </div>    
      </div>          
    </nav> -->
  </header>
  <!-- End header section -->
 
  @yield('content')



  <!-- Start Footer -->
  <footer id="mu-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="mu-footer-area">
           <div class="mu-footer-social">
            <a href="{!! route('frontend_faq')!!}" style="padding-right:50px">FAQ</a>
            <a href="https://wa.me/+6281218824244"><span class="fa fa-whatsapp"></span></a>
            <a href="//instagram.com/sehatmainbola"><span class="fa fa-instagram"></span></a>
            <a href="{!! route('frontend_rules_point')!!}" style="padding-right:57px">Poin</a>
          </div>
          <div class="mu-footer-copyright">
            <p>Sehatmainbola establish in 2018</p>
          </div>         
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->
  
  

  </body>
</html>

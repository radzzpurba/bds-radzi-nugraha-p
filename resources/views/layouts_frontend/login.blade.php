<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>JSC | @yield('title')</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/style_core.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <style type="text/css">
       
    </style>

</head>
    <?php
        $location=asset('img/tms_login.png'); 
    ?>
    <body class="login">
        @include('element.header')
            <div style="margin-top:50px;"></div>
            
            <main class="main">
                 @yield('content')
            
            </main>

    </body>
</html>

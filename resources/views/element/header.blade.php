<header class="app-header navbar" style="background-color:#2f353a;">
  <ul style="list-style-type: none;margin-top:2px;margin-left:-20px;">
    @guest
    @else
      <li>
        <a href="/" >
          <img src="{{ asset('uploads/'.$org['image']) }}" alt="organization logo" height="50px">
        </a>
      </li>
    @endguest
  </ul>
         

  <ul class="nav navbar-nav ml-auto" style="margin-right:10px;">

    <li class="nav-item dropdown">
      <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
          <li class="nav-item">
            @guest
            @else 
            @endguest
          </li>
          <li class="nav-item dropdown">
            @guest
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><span>Menu <span class="caret"></span></span>
              </a>
            @else
              <a style="color:white" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><span>  {{ Auth::user()->name }} <span class="caret"></span></span>
              </a>
            @endguest
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
          </li>
        </ul>
    </li>
  </ul>
</header>
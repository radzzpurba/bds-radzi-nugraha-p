@extends('layouts_frontend.app')
@section('title','Beranda')

@section('content')


   <!-- Start slider  -->
  <section id="mu-slider" style="margin-top:60px;margin-bottom:5px;">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
            <div class="item active">
              @if($image_one['name']!="")
                <a href="{!! $image_one['url']!!}" ><img src="{{ asset('uploads/banner/'.$image_one['name']) }}" alt="{!! $image_one['alt']!!}"></a>
              @endif
            </div>
            <div class="item">
              @if($image_two['name']!="")
                <a href="{!! $image_two['url']!!}" ><img src="{{ asset('uploads/banner/'.$image_two['name']) }}" alt="{!! $image_two['alt']!!}"></a>
                
              @endif
            </div>
            <div class="item">
              @if($image_three['name']!="")
                <a href="{!! $image_three['url']!!}" ><img src="{{ asset('uploads/banner/'.$image_three['name']) }}" alt="{!! $image_three['alt']!!}"></a>
                
              @endif
            </div>


        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <!--<span  class="fa fa-chevron-left" aria-hidden="true"></span>-->
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
           <!--<span class="fa fa-chevron-right" aria-hidden="true"></span>-->
        </a>
    </div>
      </div>
    </div>
   
  </section>
  <!-- End slider  -->
 

  <!-- Start Subscription section -->
  <section id="mu-subscription" style="padding:10px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-subscription-area">
            <center><span style="font-size:20px"> Ada Kloter > Transfer > Main (Senang + Sehat + Berhadiah)</span></center>   
          </div>
        </div>
      </div>
    </div>    
  </section>


  <!-- End Chef Section -->

  <!-- Start Counter Section -->
  <section id="mu-counter">
    <div class="mu-counter-overlay" style="padding:10px;">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="mu-counter-area">
            <ul class="mu-counter-nav">
              <li class="col-md-6 col-sm-6 col-xs-6">
                <div class="mu-single-counter">
                  <p>Terselenggara</p>
                  <h4><span class="counter">{!! $totalMatch!!}</span><sup>X</sup></h4>
                  <p>Match</p>
                </div>
              </li>
              <li class="col-md-6 col-sm-6 col-xs-6">
                <div class="mu-single-counter">
                  <p>Lebih dari</p>
                  <h4><span class="counter">140</span></h4>
                  <p>Join</p>
                </div>
              </li>
            </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Counter Section --> 



  <section id="mu-chef" style="padding:10px;"> 
    <div class="container">
      <div class="row">
        <div class="col-md-9">

        <?php if($totalEvents!=0): ?>
        <center> <h1>Jadwal Open Kloter Berikutnya ({!! $totalEvents!!} Matches)</h1></center>
          <div class="row" >

            @foreach($events as $ev)
            <?php 
              $playerLeft = $ev['quota_player']-$ev['confirmed_player'];
              $gkLeft = $ev['quota_gk']-$ev['confirmed_gk'];

              $statusQuota = "notfull";
              if($playerLeft==0 AND $gkLeft==0) $statusQuota="full";
            ?>
              <div class="col-md-4">
                <div class="panel panel-primary" style="margin-top:30px;">
                  <div class="panel-heading" style="color:white;"><center><strong>{!! $ev['location']!!}</strong><br /> {!! date('d M Y',strtotime($ev['event_date']))!!}, {!! $ev['event_time']!!} </center></div>
                  <div class="list-group">

                     <table class="table">
                        <tr><td><i class="fa fa-futbol-o" aria-hidden="true"></i></td><td>@if($ev['type_match']=="mini_soccer") Minisoccer (7 vs 7) @else Lapangan Bola Gede (11 vs 11) @endif</td></tr>
                        <tr><td><i class="fa fa-home" aria-hidden="true"></i></td><td>{!! $ev['location']!!}</td></tr>
                        <tr><td><i class="fa fa-map-marker" aria-hidden="true"></i></td><td><a href="{!! $ev['gmaps']!!}"><u>{!! $ev['address']!!}</u></a></td></tr>

                        <tr><td><i class="fa fa-calendar" aria-hidden="true"></i></td><td>{!! date('d M Y',strtotime($ev['event_date']))!!}, {!! $ev['event_time']!!}</td></tr>
                        <tr><td><i class="fa fa-money" aria-hidden="true"></i></td><td>Rp <?php echo number_format($ev['price'],0,",","."); ?></td></tr>
                        <tr><td><i class="fa fa-users" aria-hidden="true"></i></td><td>{!! $ev['quota']!!} Orang, @if($statusQuota!="full") (Sisa kuota <?php echo $playerLeft;?> Pemain + <?php echo $gkLeft; ?> GK) @else (Kuota full)@endif</td></tr>
                        <tr><td><i class="fa fa-whatsapp" aria-hidden="true"></i></td><td>{!! $ev['operator']!!} (<a href="https://wa.me/{!! $ev['phone']!!}"><u>{!! $ev['phone']!!}</u></a>)</td></tr>
                        @if(isset($ev['notes']))
                          <tr><td><i class="fa fa-file-text" aria-hidden="true"></i></td><td>Notes: {!! $ev['notes']!!}</td></tr>
                        @endif
                     </table>
                     <a href="{!! route('invoice_register',['event_id' => $ev['generated_id']])!!}" style="margin-top:-15px;border-top-right-radius:0;border-top-left-radius:0" class="list-group-item list-group-item-info"><center><i class="fa fa-sign-in fa-lg" aria-hidden="true"></i> Join Match </center></a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        <?php else:?>
          <center> <h1>Libur dulu ya</h1></center>
        <?php endif; ?>
        </div>
        <div class="col-md-3">
          <center> <h1>Photos</h1></center>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-success" style="margin-top:30px;">
                <div class="panel-heading" style="color:white;"><center>Google Drive Link</center></div>
                <div class="list-group">
                   @foreach($link as $l)
                    <a href="{!! $l['gdrive']!!}" style="border-top-right-radius:0;border-top-left-radius:0" class="list-group-item"><center><i class="fa fa-download" aria-hidden="true"></i> {!! $l['location']!!} <br />{!! date('d M Y',strtotime($l['event_date']))!!}, {!! $l['event_time']!!}</center></a>
                   @endforeach
                   <div class="panel-heading list-group-item list-group-item-success"><center>*Notes: Foto dalam Gdrive akan dihapus dalam waktu tiga minggu. Segera download fotonya Bosku.</center></div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </section>

  <!-- Start Chef Section -->
  <section id="mu-chef" style="background-color: rgba(0, 0, 0, 0.8);padding:20px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-chef-area">
            <div class="mu-title">
              <h2 style="color:white">Lapangan tempat biasa main</h2>
            </div>
            <div class="mu-chef-content">
              <ul class="mu-chef-nav">
                <li>
                  <div class="mu-single-chef">
                      <div class="panel panel-warning">
                        <div class="list-group">
                          <div class="list-group-item">Serenia Mansion - Cilandak</div>
                          <div class="list-group-item">F7 Mini Soccer - Tanjung Barat</div>
                          <div class="list-group-item">Simprug Pertamina - Simprug</div>
                        </div>
                      </div>
                    <div class="mu-single-chef-info">
                      <h4>Jakarta Selatan</h4>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="mu-single-chef">
                      <div class="panel panel-warning">
                        <div class="list-group">
                          <div class="list-group-item">Lapangan A B C Senayan - GBK </div>
                          <div class="list-group-item">Arcici Sport Center - Cempaka Putih</div>
                        </div>
                      </div>
                    <div class="mu-single-chef-info">
                      <h4>Jakarta Pusat</h4>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="mu-single-chef">
                      <div class="panel panel-warning">
                        <div class="list-group">
                          <div class="list-group-item">De'Kings Arena - Lubang Buaya</div>
                          <div class="list-group-item">Vidi Arena - Pulomas</div>
                        </div>
                      </div>
                    <div class="mu-single-chef-info">
                      <h4>Jakarta Timur</h4>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="mu-single-chef">
                      <div class="panel panel-warning">
                        <div class="list-group">
                          <div class="list-group-item">Serenia Mansion - Cilandak</div>
                          <div class="list-group-item">F7 Mini Soccer - Tanjung Barat</div>
                          <div class="list-group-item">Simprug Pertamina - Simprug</div>
                        </div>
                      </div>
                    <div class="mu-single-chef-info">
                      <h4>Jakarta Selatan</h4>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="mu-single-chef">
                      <div class="panel panel-warning">
                        <div class="list-group">
                          <div class="list-group-item">De'Kings Arena - Lubang Buaya</div>
                          <div class="list-group-item">Vidi Arena - Pulomas</div>
                        </div>
                      </div>
                    <div class="mu-single-chef-info">
                      <h4>Jakarta Timur</h4>
                    </div>
                  </div>
                </li>

  
                                    
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>





 



@endsection

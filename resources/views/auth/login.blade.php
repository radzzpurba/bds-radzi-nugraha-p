@extends('layouts_frontend.login')
@section('title','Login')
@section('content')

<?php 
$email="null";
$password = "null";

if(isset($_GET['email'])) $email=$_GET['email'];
if(isset($_GET['password'])) $password=$_GET['password'];

;?>

<style type="text/css">

    .content-login{
        width: 450px;

        margin: 0 auto;


        margin-bottom: 0;

        padding: 20px;


        padding-top: 20px;

        padding-bottom: 15px;
    }
    
</style>


    <?php
        $location_login=asset('img/bg-white-lock.png'); 
    ?>

            <div class="content-login" style=" background-image: url('{{$location_login}}');margin-top:150px">
                <center><h3 style="font-weigth" class="form-title">Login to your account</h3></center><br />
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-2 col-form-label text-md-right"><i class="fa fa-fw fa-user"></i></label>

                            <div class="col-md-10">
                                <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" <?php if($email!="null"): ?> value="{!! $email!!}" <?php else: ?> value="{{ old('email') }}" <?php endif; ?> required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right"><i class="fa fa-fw fa-lock"></i></label>

                            <div class="col-md-10">
                                <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" <?php if($email!="null"): ?> value="{!! $password!!}" <?php endif; ?> required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                       
                        <button type="submit" class="btn btn-primary float-right">
                            <i class="fa fa-btn fa-sign-in"></i> Login
                        </button>
                    </form>
                    <br/>
                    <br/>
                    <br/>
        </div> 
@endsection

@extends('layouts.app')
@section('title','Ubah Data Diri')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Ubah Data Diri') }}</div>

                <div class="card-body">
                    <form enctype="multipart/form-data" id="formEdit" role="form" method="POST" action="" >

                                <div class= "md-12">
                                    <input type="hidden" name="id" id="idEdit" value="{!! Auth::user()->generated_id!!}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id ="csrfTokenEdit">
                                    <br />
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Nama
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="name" id="nameEdit" class="form-control" value="{!! Auth::user()->name!!}">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Email
                                    </div>
                                    <div class="col-md-9">
                                        <input type="email" name="email" id="emailEdit" class="form-control" value="{!! Auth::user()->email!!}">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Password
                                    </div>
                                    <div class="col-md-9">
                                        <input type="password" name="password" id="passwordEdit" class="form-control" value="{!! Auth::user()->password_string!!}">
                                        <small class="help-block"></small><br />
                                        <span class="text-danger">*biarkan jika tidak ingin mengganti password</span>
                                    </div>
                                </div>  
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Konfirmasi Password
                                    </div>
                                    <div class="col-md-9">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{!! Auth::user()->password_string!!}">
                                        <small class="help-block"></small><br />
                                        <span class="text-danger">*biarkan jika tidak ingin mengganti password</span>
                                    </div>
                                </div>  
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Instagram
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="instagram" id="instagramEdit" class="form-control" value="{!! Auth::user()->instagram!!}">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        No HP
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" id="phoneEdit" class="form-control" value="{!! Auth::user()->phone!!}">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                            </form> 
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <span id="resultEditId" class="float-right" style="font-size:15px;color:green"></span>
                                </div>
                                 <div class="col-md-6">
                                    <button type="button" class="btn btn-primary float-right" id="buttonEditId">Simpan</button>    
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*--- EDIT ---*/
            $(document).on("click", "#buttonEditId", function() {
                return $.ajax({
                  url: "{!! route('user_updateself')!!}",
                  type: 'POST',
                  data: $('#formEdit').serialize(),
                  dataType: 'json',
                  beforeSend: (function() {
                    $('#buttonEditId').prop('disabled', true);
                    $('#closeEditId').prop('disabled', true);
                    $('#closeEditId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
                    $('#buttonEditId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
                  }),
                  complete: (function() {
                    $('#buttonEditId').prop('disabled', false);
                    $('#buttonEditId').html('Simpan');
                    $('#closeEditId').prop('disabled', false);
                    $('#closeEditId').html('Tutup');
                  }),
                  success: function(result) {
                    if (result.status == 'success') {

                      $('#resultEditId').html('');
                      $('#resultEditId').html('Update Data Sukses!');
                    }
                    else{
                        $.each(result.message, function (key, value) {
                            var input = '#formEdit input[name=' + key + ']';
                            $(input + '+small').text(value);
                            $(input).parent().addClass('text-danger');

                            var select = '#formEdit select[name=' + key + ']';
                            $(select + '+small').text(value);
                            $(select).parent().addClass('text-danger');
                        });
                    }
                  }
                });
            });

            $('#edit').on('hidden.bs.modal', function() {
                clearForm();
                datatable.draw();
            });

            $('#edit').on('shown.bs.modal', function() {
                setTimeout(function (){
                    $('#emailEdit').focus();
                }, 1000);
            });


        /*--- END EDIT ---*/
</script>
@endsection

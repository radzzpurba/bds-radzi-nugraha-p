@extends('layouts.app')
@section('title','User')

@section('content')
@include('element.js-date-n-choosen')

    <center>
        <h3><span class="newTitle">User</span></h3>
    </center>
    
	   <a type="button" id="buttonAddModal" class="btn btn-info btn-sm text-white mb-2 ml-1 px-4 float-right" data-toggle="modal" data-target="#add"><i class="fa fa-plus" aria-hidden="true"></i> Add User </a>

     <a id="buttonEditModal" data-toggle="modal" data-target="#edit"></a>

	<br />
	<br />
	<table id="tableId" class="table table-hover bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>


    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                            <form enctype="multipart/form-data" id="formAdd" role="form" method="POST" action="" >

                                <div class= "md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id ="csrfTokenAdd">
                                    <input type="hidden" name="meta_insert" value="manual" id ="meta_insert">
                                    <input type="hidden" name="inserted_by" value="<?php echo Auth::user()->name;?>" id ="inserted_by">
                                    <br />
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Nama
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="name" id="name" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                             
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Email
                                    </div>
                                    <div class="col-md-9">
                                        <input type="email" name="email" id="email" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Password
                                    </div>
                                    <div class="col-md-9">
                                        <input type="password" name="password" id="password" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        {{$menu['hub']}}
                                    </div>
                                    <div class="col-md-9">
                                       <select id="hub" name="hub[]" class="form-control" multiple>
                                            @foreach($hubs as $h)
                                                <option value="{{$h['id']}}">{{$h['name']}}</option>
                                            @endforeach
                                        </select>
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                 

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Phone
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" id="phone" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Status
                                    </div>
                                    <div class="col-md-9">
                                        <select name="status" id="status" class="form-control">
                                            <option value="">Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Suspend">Suspend</option>
                                        </select>
                                        <small class="help-block"></small>
                                    </div>
                                </div>


                                

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <center style="font-size:15px"><strong>Role</strong></center>
                                    </div>
                                </div>

                            

                                <div class="row form-group">
                                    
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#vehicleTab" aria-expanded="true" aria-controls="vehicleTab"><i class="fa fa-mobile" aria-hidden="true"></i> {{$menu['vehicle']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="vehicleTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-list"  id="vehicle-listCheckAdd"> {{$menu['vehicle']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-add"  id="vehicle-addAdd"> {{$menu['vehicle']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-edit"  id="vehicle-editAdd"> {{$menu['vehicle']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#hubTab" aria-expanded="true" aria-controls="hubTab"><i class="fa fa-university" aria-hidden="true"></i> {{$menu['hub']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="hubTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-list"  id="hub-listCheckAdd"> {{$menu['hub']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-add"  id="hub-addAdd"> {{$menu['hub']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-edit"  id="hub-editAdd"> {{$menu['hub']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#zoneTab" aria-expanded="true" aria-controls="zoneTab"><i class="fa fa-map-signs" aria-hidden="true"></i> {{$menu['zone']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="zoneTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-list"  id="zone-listCheckAdd"> {{$menu['zone']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-add"  id="zone-addAdd"> {{$menu['zone']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-edit"  id="zone-editAdd"> {{$menu['zone']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#addressTab" aria-expanded="true" aria-controls="addressTab"><i class="fa fa-address-book-o" aria-hidden="true"></i> Address</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="addressTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-list"  id="address-listCheckAdd"> Address List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-add"  id="address-addAdd"> Address Import
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-edit"  id="address-editAdd"> Address Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#userTab" aria-expanded="true" aria-controls="userTab"><i class="fa fa-user" aria-hidden="true"></i> User</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="userTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-list"  id="user-listCheckAdd"> User List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-add"  id="user-addAdd"> User Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-edit"  id="user-editAdd"> User Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#clientTab" aria-expanded="true" aria-controls="clientTab"><i class="fa fa-group" aria-hidden="true"></i> {{$menu['client']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="clientTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-list"  id="client-listCheckAdd"> {{$menu['client']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-add"  id="client-addAdd"> {{$menu['client']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-edit"  id="client-editAdd"> {{$menu['client']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#jobTab" aria-expanded="true" aria-controls="jobTab"><i class="fa fa-suitcase" aria-hidden="true"></i> {{$menu['job']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="jobTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-list"  id="job-listCheckAdd"> {{$menu['job']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-import"  id="job-importCheckAdd"> {{$menu['job']}} Import
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-assign"  id="job-assignCheckAdd"> {{$menu['job']}} Assign
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-unassign"  id="job-unassignCheckAdd"> {{$menu['job']}} Un-assign
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#otherTab" aria-expanded="true" aria-controls="otherTab"><i class="fa fa-gear" aria-hidden="true"></i> Other</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="otherTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="map-list"  id="map-listCheckAdd"> {{$menu['map']}} Page
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="org-list"  id="org-listCheckAdd"> Organization
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="summary-list"  id="summary-listCheckAdd"> Summary
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="manualpod-list"  id="manualpod-listCheckAdd"> {{$menu['manual_pod']}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="failedreason-list"  id="failedreason-listCheckAdd"> {{$menu['failed_reason']}}
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </form>    
                       			

                           
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-right"  id="closeAddId" data-dismiss="modal" >Tutup</button> 
                        <button type="button" class="btn btn-primary float-right" id="buttonAddId">Simpan</button>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <span id="resultAddId" style="font-size:15px;color:green"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                            <form enctype="multipart/form-data" id="formEdit" role="form" method="POST" action="" >

                                <div class= "md-12">
                                    <input type="hidden" name="id" id="idEdit">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id ="csrfTokenEdit">
                                    <br />
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Nama
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="name" id="nameEdit" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Phone
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="phone" id="phoneEdit" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Email
                                    </div>
                                    <div class="col-md-9">
                                        <input type="email" name="email" id="emailEdit" class="form-control">
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Password
                                    </div>
                                    <div class="col-md-9">
                                        <input type="password" name="password" id="passwordEdit" class="form-control">
                                        <small class="help-block"></small>
                                        <span class="text-danger">*fill this field, if only want to change the password</span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        {{$menu['hub']}}
                                    </div>
                                    <div class="col-md-9">
                                       <select id="hubEdit" name="hub[]" class="selectMulti form-control" multiple>
                                            @foreach($hubs as $h)
                                                <option value="{{$h['id']}}">{{$h['name']}}</option>
                                            @endforeach
                                        </select>
                                        <small class="help-block"></small>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3 float-left">
                                        Status
                                    </div>
                                    <div class="col-md-9">
                                        <select name="status" id="statusEdit" class="form-control">
                                            <option value="">Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Suspend">Suspend</option>
                                        </select>
                                        <small class="help-block"></small>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <center style="font-size:15px"><strong>Role</strong></center>
                                    </div>
                                </div>

                            

                                <div class="row form-group">
                                    
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#vehicleTab" aria-expanded="true" aria-controls="vehicleTab"><i class="fa fa-mobile" aria-hidden="true"></i> {{$menu['vehicle']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="vehicleTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-list"  id="vehicle-listCheckEdit"> {{$menu['vehicle']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-add"  id="vehicle-addCheckEdit"> {{$menu['vehicle']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="vehicle-edit"  id="vehicle-editCheckEdit"> {{$menu['vehicle']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#hubTab" aria-expanded="true" aria-controls="hubTab"><i class="fa fa-university" aria-hidden="true"></i> {{$menu['hub']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="hubTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-list"  id="hub-listCheckEdit"> {{$menu['hub']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-add"  id="hub-addCheckEdit"> {{$menu['hub']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="hub-edit"  id="hub-editCheckEdit"> {{$menu['hub']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#zoneTab" aria-expanded="true" aria-controls="zoneTab"><i class="fa fa-map-signs" aria-hidden="true"></i> {{$menu['zone']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="zoneTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-list"  id="zone-listCheckEdit"> {{$menu['zone']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-add"  id="zone-addCheckEdit"> {{$menu['zone']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="zone-edit"  id="zone-editCheckEdit"> {{$menu['zone']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#addressTab" aria-expanded="true" aria-controls="addressTab"><i class="fa fa-address-book-o" aria-hidden="true"></i> Address</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="addressTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-list"  id="address-listCheckEdit"> Address List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-add"  id="address-addCheckEdit"> Address Import
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="address-edit"  id="address-editCheckEdit"> Address Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#userTab" aria-expanded="true" aria-controls="userTab"><i class="fa fa-user" aria-hidden="true"></i> User</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="userTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-list"  id="user-listCheckEdit"> User List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-add"  id="user-addCheckEdit"> User Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="user-edit"  id="user-editCheckEdit"> User Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#clientTab" aria-expanded="true" aria-controls="clientTab"><i class="fa fa-group" aria-hidden="true"></i> {{$menu['client']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="clientTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-list"  id="client-listCheckEdit"> {{$menu['client']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-add"  id="client-addCheckEdit"> {{$menu['client']}} Add
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="client-edit"  id="client-editCheckEdit"> {{$menu['client']}} Edit
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#jobTab" aria-expanded="true" aria-controls="jobTab"><i class="fa fa-suitcase" aria-hidden="true"></i> {{$menu['job']}}</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="jobTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-list"  id="job-listCheckEdit"> {{$menu['job']}} List
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-import"  id="job-importCheckEdit"> {{$menu['job']}} Import
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-assign"  id="job-assignCheckEdit"> {{$menu['job']}} Assign
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="job-unassign"  id="job-unassignCheckEdit"> {{$menu['job']}} Un-assign
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <center>
                                            <div class="card card-body" style="padding-top:7px;padding-bottom:7px">
                                                <a style="color:black;" data-toggle="collapse" href="#otherTab" aria-expanded="true" aria-controls="otherTab"><i class="fa fa-gear" aria-hidden="true"></i> Other</a>
                                            </div>
                                        </center>

                                        <div class="collapse multi-collapse" id="otherTab" style="margin-top:-15px">
                                            <div class="card card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="map-list"  id="map-listCheckEdit"> {{$menu['map']}} Page
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="org-list"  id="org-listCheckEdit"> Organization
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="summary-list"  id="summary-listCheckEdit"> Summary
                                                    </div>
                                                </div><br />
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="manualpod-list"  id="manualpod-listCheckEdit"> {{$menu['manual_pod']}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="checkbox" name="role[]" value="failedreason-list"  id="failedreason-listCheckEdit"> {{$menu['failed_reason']}}
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-right"  id="closeEditId" data-dismiss="modal" >Tutup</button> 
                        <button type="button" class="btn btn-primary float-right" id="buttonEditId">Simpan</button>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <span id="resultEditId" style="font-size:15px;color:green"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    <script type="text/javascript">
    	
    	$(document).ready(function() {
	      generateTable();

	    });

    	/*--- ADD ---*/
	    	$('#add').on('hidden.bs.modal', function() {
		        clearForm();
		        datatable.draw();
		    });

            $(document).on("click", "#buttonAddModal", function() {
                 setTimeout(function() {
                    $('#name').focus();
                    $("#hub").chosen({
                      no_results_text: "nothing found!",
                      placeholder_text_multiple: "All ",
                      height: "34px"
                    });
                },300);
                
            });

		  

		    $(document).on("click", "#buttonAddId", function() {

		        return $.ajax({
		          url: "{!! route('user_add')!!}",
		          type: 'POST',
		          data: $('#formAdd').serialize(),
		          dataType: 'json',
		          beforeSend: (function() {
		            $('#buttonAddId').prop('disabled', true);
		            $('#closeAddId').prop('disabled', true);
		            $('#closeAddId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
		            $('#buttonAddId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
		          }),
		          complete: (function() {
		            $('#buttonAddId').prop('disabled', false);
		            $('#buttonAddId').html('Simpan');
		            $('#closeAddId').prop('disabled', false);
		            $('#closeAddId').html('Tutup');
                    $("#closeAddId").click();
                    datatable.draw();
                    
                    
		          }),
		          success: function(result) {
		            if (result.status == 'success') {
		              $('#resultAddId').html('');
		              document.getElementById('resultAddId').value = result.message;
                     

		             
		            }
		            else{
		                $.each(result.message, function (key, value) {
		                    var input = '#formAdd input[name=' + key + ']';
		                    $(input + '+small').text(value);
		                    $(input).parent().addClass('text-danger');

		                    var select = '#formAdd select[name=' + key + ']';
		                    $(select + '+small').text(value);
		                    $(select).parent().addClass('text-danger');
		                    
		                });
		            }
		          }
		        });
		    });

		/*--- END ADD ----*/

        /*--- EMAIL ---*/
            $('#emailModal').on('hidden.bs.modal', function() {
                clearForm();
                datatable.draw();
            });

            $(document).on("click", ".buttonEmail", function() {
                var buttonID, resultSplit;
                buttonID = $(this).data('id');
                resultSplit = buttonID.split("|||");
                $(".modal-body #idEmail").val(resultSplit[0]);
                $(".modal-body #nameEmail").val(resultSplit[1]);
                $(".modal-body #emailEmail").val(resultSplit[2]);
                $('#emailModal').modal('show');
                 
                return void 0;
            });

            $(document).on("click", "#buttonEmailId", function() {
                        return $.ajax('', {
                          url: "{!! route('user_email')!!}",
                          data: $('#formEmail').serialize(),
                          dataType: 'json',
                          async: false,
                          type: 'post',
                          beforeSend: (function() {
                            $('#buttonEmailId').html('<i class="fa fa-spin fa-refresh fa-fw fa-2x"></i>');
                            $('#closeEmailId').html('<i class="fa fa-spin fa-refresh fa-fw fa-2x"></i>');
                            $('#buttonEmailId').attr('disabled', 'disabled');
                            $('#closeEmailId').attr('disabled', 'disabled');
                            return void 0;
                          }),
                          complete: (function() {
                            $('#buttonEmailId').html('Kirim');
                            $('#closeEmailId').html('Tutup');
                            $('#buttonEmailId').removeAttr("disabled");
                            $('#closeEmailId').removeAttr("disabled");
                            return void 0;
                          }),
                          success: function(result) {
                            if(result.status=='success'){
                                $('#resultEmailId').html('');
                                document.getElementById('resultEditId').value = result.status;
                            }

                          }
                        });
                    
            });

        /*--- END EMAIL ---*/

		/*--- EDIT ---*/
			$(document).on("click", "#buttonEditId", function() {
		        return $.ajax({
		          url: "{!! route('user_edit')!!}",
		          type: 'POST',
		          data: $('#formEdit').serialize(),
		          dataType: 'json',
		          beforeSend: (function() {
		            $('#buttonEditId').prop('disabled', true);
		            $('#closeEditId').prop('disabled', true);
		            $('#closeEditId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
		            $('#buttonEditId').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i>');
		          }),
		          complete: (function() {
		            $('#buttonEditId').prop('disabled', false);
		            $('#buttonEditId').html('Simpan');
		            $('#closeEditId').prop('disabled', false);
		            $('#closeEditId').html('Tutup');
                    $("#closeEditId").click();
                    
                    datatable.draw();

		          }),
		          success: function(result) {
		            if (result.status == 'success') {
		              $('#resultEditId').html('');
		              document.getElementById('resultEditId').value = result.message;
		              
		            }
		            else{
		                $.each(result.message, function (key, value) {
		                    var input = '#formEdit input[name=' + key + ']';
		                    $(input + '+small').text(value);
		                    $(input).parent().addClass('text-danger');

		                    var select = '#formEdit select[name=' + key + ']';
		                    $(select + '+small').text(value);
		                    $(select).parent().addClass('text-danger');
		                });
		            }
		          }
		        });
		    });

		    $('#edit').on('hidden.bs.modal', function() {
		        clearForm();
		        datatable.draw();
		    });


		    $(document).on("click", ".buttonEdit", function() {
                //

                document.getElementById("buttonEditModal").click();

		        var buttonID, resultSplit;
		        buttonID = $(this).data('id');
		        resultSplit = buttonID.split("|||");
		        $(".modal-body #idEdit").val(resultSplit[0]);
		        $(".modal-body #nameEdit").val(resultSplit[1]);
                $(".modal-body #phoneEdit").val(resultSplit[2]);
		        $(".modal-body #emailEdit").val(resultSplit[3]);
		        $(".modal-body #statusEdit").val(resultSplit[4]);
		        $('.modal-body #statusEdit').trigger("chosen:updated");
                role = resultSplit[5].split(" | ");
                  for (index = i = 0, len = role.length; i < len; index = ++i) {
                    element = role[index];
                    $("#" + element + "CheckEdit").prop("checked", true);
                  }

                hub = resultSplit[6].split(" | ");
                $(".modal-body #hubEdit").val(hub);
                $('.modal-body #hubEdit').trigger("chosen:updated");

                setTimeout(function() {
                    $('#nameEdit').focus();
                    $("#hubEdit").chosen({
                      no_results_text: "nothing found!",
                      placeholder_text_multiple: "All ",
                      height: "34px"
                    });
                },300);

		        
		        return void 0;
		    });

		/*--- END EDIT ---*/

		/*--- IMPORT ---*/
			$('#import').on('hidden.bs.modal', function() {
		        clearForm();
		        datatable.draw();
		    });

			$(document).on("click", "#submitId", function() {
		            var checkFile;
		            checkFile = document.getElementById('uploadBox').value;

		            if(checkFile==''){
		                alert('Pilih file terlebih dahulu');
		                $('#uploadBox').focus();
		            } 
		            else{

		                return $.ajax('', {
		                  url: "{!! route('user_import')!!}",
		                  data: new FormData($("#formId")[0]),
		                  dataType: 'json',
		                  async: false,
		                  type: 'post',
		                  processData: false,
		                  contentType: false,
		                  beforeSend: (function() {
		                    $('#submitId').html('<i class="fa fa-spin fa-refresh fa-fw fa-2x"></i>');
		                    $('#closeId').html('<i class="fa fa-spin fa-refresh fa-fw fa-2x"></i>');
		                    $('#submitId').attr('disabled', 'disabled');
		                    $('#closeId').attr('disabled', 'disabled');
		                    return void 0;
		                  }),
		                  complete: (function() {
		                    $('#submitId').html('Proses');
		                    $('#closeId').html('Tutup');
		                    $('#submitId').removeAttr("disabled");
		                    $('#closeId').removeAttr("disabled");
		                    return void 0;
		                  }),
		                  success: function(result) {
		                    var color,message
		                    if(result.data.status=='failed'){
		                        color='red';
		                        message='Template tidak sesuai';
		                        $(".modal-body #detailResultImport").html("");
		                        $(".modal-body #detailResultImport").html('<span style="color:'+color+'">'+result.data.message+'</span>');

		                    }
		                    else{
		                        color='green';
		                        message='Import berhasil!';
		                        $(".modal-body #detailResultImport").html("");
		                        $(".modal-body #detailResultImport").html('<span style="color:'+color+'">Total: '+result.data.valid+'</span>');
		                    }

		                    $(".modal-body #resultImport").html('');
		                    $(".modal-body #resultImport").html('<span style="color:'+color+'">'+message+'</span>');
		                  }
		                });
		            }
		    });	

		/*--- END IMPORT ---*/

		/*--- HELPER ---*/
			function clearForm(){
			    $('#name').val('');
			    $('#nameEdit').val('');
                $('#nameEmail').val('');
                $('#emailEmail').val('');
                $('#resultEmailId').val('');
			    $('#email').val('');
			    $('#emailEdit').val('');
			    $('#instagram').val('');
			    $('#instagramEdit').val('');
			    $('#phone').val('');
			    $('#phoneEdit').val('');
			    $('#point').val('');
			    $('#pointEdit').val('');
			    $('#saldo').val('');
			    $('#saldoEdit').val('');
			    $('#uploadBox').val('');
			    $('#type').val('');
      			$('.modal-body #type').trigger("chosen:updated");
			    $('#typeEdit').val('');
      			$('.modal-body #typeEdit').trigger("chosen:updated");
			    $(".modal-body #detailResultImport").html("");
			    $(".modal-body #resultImport").html('');
			    $('input+small').text('');
			    $('select+small').text('');
			    $('input').parent().removeClass('text-danger');
			    $('select').parent().removeClass('text-danger');
		    }

		/*--- END HELPER ---*/

		/*--- TABLE ---*/
		    function generateTable(){
		        datatable = $('#tableId').DataTable({
		            processing: true,
		            serverSide: true,
		            orderCellsTop: true,
		            fixedHeader: true,
                    scrollX: true,
		            ajax: {
		                'type': 'GET',
		            },
		            columns: [
                        { data: 'name', name: 'name' },
		                { data: 'status', name: 'status' },
		                { data: 'phone', name: 'phone' },
                        { data: 'email', name: 'email' },
                        { data: 'hub_name', name: 'hub_name' },
		                { data: 'role', name: 'role' },
		                { data: 'actions', name: 'actions' }
		            ]
		        });
		    }
	    /*--- END TABLE ---*/



    </script>

@endsection

@extends('layouts.app')
@section('title','Rumah Sakit')

@section('content')
    <br />
    <br />
    <br />

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
              <h5 class="card-header  bg-info"><i class="fa fa-spin fa-refresh fa-lg fa-fw"></i> Redirect</h5>
              <div class="card-body">
                <center>
                    Fetching Data Rumah Sakit telah selesai. <br /> 
                    Anda akan di-redirect ke halaman List Rumah Sakit<br />
                <center>
              </div>
            </div>
        </div>
        <div class="col-md-3"></div>

    </div>

    <script type="text/javascript">
        setTimeout(function () {    
            window.location.href = "{!! route('rs_list')!!}"; 
        },6500);
    </script>
   

@endsection

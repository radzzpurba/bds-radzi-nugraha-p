@extends('layouts.app')
@section('title','Rumah Sakit')

@section('content')
@include('element.js-date-n-choosen')

    
    <center>
        <h3><span class="newTitle">List Rumah Sakit</span></h3>
    </center>
    
    <br />


    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link" href='{!! route('kelurahan_list')!!}'>List Kelurahan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href='#'>List Rumah Sakit</a>
      </li>
    </ul>
    <br />

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id ="csrfToken">
    
    <a class="btn btn-success" href='{!! route('rs_getRumahSakitAPI')!!}' id="buttonGetData" ><i class="fa fa-exchange" aria-hidden="true"></i> Get Data Rumah Sakit</a>
    

    <br />
    <br />

    <table id="tableId" class="table table-hover bordered" cellspacing="0" width="100%">
        <thead>
            <tr>   
                <th>ID Rumah Sakit</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Alamat</th>
                <th>Kode Pos</th>
                <th>Nomor Telepon</th>
                <th>Faximile</th>
                <th>Website</th>
                <th>Email</th>
                <th>Kode Kota</th>
                <th>Kode Kecamatan</th>
                <th>Kode Kelurahan</th>
                <th>Created Date</th>
                <th>Updated Date</th>
            </tr>
        </thead>
    </table>

    <script type="text/javascript">
  
            $(document).ready(function() {
                $("#notif_fetch").hide(); 
                generateTable();
            });

            $(document).on("click", "#buttonGetData", function() {
                var element = document.getElementById("buttonGetData");
                element.classList.remove("btn-success");
                element.classList.add("btn-danger");

                $('#buttonGetData').prop('disabled', true);
                $('#buttonGetData').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i> Fething data is in progress');
                
            });

       

 
            /*--- TABLE ---*/
                function generateTable(){
                    var src = "<?php echo asset('img/loading_bar.gif'); ?>";

                    datatable = $('#tableId').DataTable({
                        processing: true,
                        serverSide: true,
                        orderCellsTop: true,
                        fixedHeader: true,
                        fixedHeader: true,
                        scrollX: true,
                        language: {
                            processing: '<img src="'+src+'">'
                        },
                        ajax: {
                            type: 'GET',
                            url: "{!! route('rs_list')!!}"
                        },   
                        columns: [
                            { data: 'id_rsu', name: 'id_rsu' },
                            { data: 'nama_rsu', name: 'nama_rsu' },
                            { data: 'jenis_rsu', name: 'jenis_rsu' },
                            { data: 'alamat', name: 'alamat' },
                            { data: 'kode_pos', name: 'kode_pos' },
                            { data: 'telepon', name: 'telepon' },
                            { data: 'faximile', name: 'faximile' },
                            { data: 'website', name: 'website' },
                            { data: 'email', name: 'email' },
                            { data: 'kode_kota', name: 'kode_kota' },
                            { data: 'kode_kecamatan', name: 'kode_kecamatan' },
                            { data: 'kode_kelurahan', name: 'kode_kelurahan' },
                            { data: 'created', name: 'created' },
                            { data: 'updated', name: 'updated' }
                        ]
                    });
                }
            /*--- END TABLE ---*/
    </script>

@endsection

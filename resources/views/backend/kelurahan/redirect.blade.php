@extends('layouts.app')
@section('title','Kelurahan')

@section('content')
    <br />
    <br />
    <br />

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card">
              <h5 class="card-header  bg-info"><i class="fa fa-spin fa-refresh fa-lg fa-fw"></i> Redirect</h5>
              <div class="card-body">
                <center>
                    Fetching Data Kelurahan telah selesai. <br /> 
                    Anda akan di-redirect ke halaman List Kelurahan<br />
                <center>
              </div>
            </div>
        </div>
        <div class="col-md-3"></div>

    </div>

    <script type="text/javascript">
        setTimeout(function () {    
            window.location.href = "{!! route('kelurahan_list')!!}"; 
        },6000);
    </script>
   

@endsection

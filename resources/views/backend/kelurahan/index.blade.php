@extends('layouts.app')
@section('title','Kelurahan')

@section('content')
    <center>
        <h3><span class="newTitle">List Kelurahan</span></h3>
    </center>
    
    <br />


    <ul class="nav nav-tabs">
        <li class="nav-item">
        <a class="nav-link active" href='#' >List Kelurahan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href='{!! route('rs_list')!!}' >List Rumah Sakit</a>
      </li>
    </ul>
    <br />

    <input type="hidden" name="_token" value="{{ csrf_token() }}" id ="csrfToken">
    
    <a class="btn btn-success" href='{!! route('kelurahan_getKelurahanAPI')!!}' id="buttonGetData" ><i class="fa fa-exchange" aria-hidden="true"></i> Get Data Kelurahan</a>
    

    <br />
    <br />

    <table id="tableId" class="table table-hover bordered" cellspacing="0" width="100%">
        <thead>
            <tr>   
                <th>Kode Provinsi</th>
                <th>Nama Provinsi</th>
                <th>Kode Kota</th>
                <th>Nama Kota</th>
                <th>Kode Kecamatan</th>
                <th>Nama Kecamatan</th>
                <th>Kode Kelurahan</th>
                <th>Nama Kelurahan</th>
                <th>Created Date</th>
                <th>Updated Date</th>
            </tr>
        </thead>
    </table>

    <script type="text/javascript">
  
            $(document).ready(function() {
                $("#notif_fetch").hide(); 
                generateTable();
            });

            $(document).on("click", "#buttonGetData", function() {
                var element = document.getElementById("buttonGetData");
                element.classList.remove("btn-success");
                element.classList.add("btn-danger");

                $('#buttonGetData').prop('disabled', true);
                $('#buttonGetData').html('<i class="fa fa-spin fa-refresh fa-lg fa-fw"></i> Fething data is in progress');
                
            });

       

 
            /*--- TABLE ---*/
                function generateTable(){
                    var src = "<?php echo asset('img/loading_bar.gif'); ?>";

                    datatable = $('#tableId').DataTable({
                        processing: true,
                        serverSide: true,
                        orderCellsTop: true,
                        fixedHeader: true,
                        fixedHeader: true,
                        scrollX: true,
                        language: {
                            processing: '<img src="'+src+'">'
                        },
                        ajax: {
                            type: 'GET',
                            url: "{!! route('kelurahan_list')!!}"
                        },   
                        columns: [
                            { data: 'kode_provinsi', name: 'kode_provinsi' },
                            { data: 'nama_provinsi', name: 'nama_provinsi' },
                            { data: 'kode_kota', name: 'kode_kota' },
                            { data: 'nama_kota', name: 'nama_kota' },
                            { data: 'kode_kecamatan', name: 'kode_kecamatan' },
                            { data: 'nama_kecamatan', name: 'nama_kecamatan' },
                            { data: 'kode_kelurahan', name: 'kode_kelurahan' },
                            { data: 'nama_kelurahan', name: 'nama_kelurahan' },
                            { data: 'created', name: 'created' },
                            { data: 'updated', name: 'updated' }
                        ]
                    });
                }
            /*--- END TABLE ---*/
    </script>

@endsection

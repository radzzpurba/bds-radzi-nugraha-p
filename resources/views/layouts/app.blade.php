<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> JSC | @yield('title')</title>

    <!-- Scripts -->


    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/coreui.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/simple-line.css') }}" rel="stylesheet">
    <link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/style_core.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    @guest
    @else
      <link rel="shortcut icon" href="{{ asset('uploads/'.$org['favicon']) }}">
    @endguest

  <style type="text/css">
    .modal-big-width{
      width: 150% !important;
      margin-left: -120px;
    }
    html,body{
      overflow-x: hidden;
    }
  </style>



</head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show brand-minimized" style="background-color:#f9f9f9">
    <div id="app">
      @include('element.header')
        <?php $active = ""; $pageName=""; ?>
            <div class="app-body">
                <div class="sidebar" style="font-size:12px">
                    <nav class="sidebar-nav">
                      <ul class="nav">
                        <li class="nav-item">
                            <?php if($pageName=="kelurahan"): $active = "active"; endif; ?>
                              <a class="nav-link <?php echo $active; ?> " href="{!! route('kelurahan_list')!!}">
                            <i class="nav-icon icon-map" aria-hidden="true"></i> List Kelurahan
                          </a>
                        </li>
                        <li class="nav-item">
                          <?php if($pageName=="rumah_sakit"): $active = "active"; endif; ?>
                          <a class="nav-link " href="{!! route('rs_list')!!}">
                            <i class="nav-icon icon-notebook" aria-hidden="true"></i> List Rumah Sakit
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link " href="{!! route('data_list')!!}" target="__blank">
                            <i class="nav-icon icon-badge" aria-hidden="true"></i> Test Result
                          </a>
                        </li>
                       
                      </ul>
                    </nav>
                    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
                  </div>
            </div>

            <main class="main">
                <br />
                <div class="ml-1 mr-1">
                    @yield('content')
                </div>
            </main>
    </div>
  </body>
</html>

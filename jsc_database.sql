-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2021 at 09:51 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jsc_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_aliases`
--

CREATE TABLE `file_aliases` (
  `id` int(11) NOT NULL,
  `external_id` varchar(80) DEFAULT 'External ID',
  `job_number` varchar(80) DEFAULT 'Order No',
  `hub_name` varchar(80) DEFAULT 'Group Name',
  `assign_to` varchar(80) DEFAULT 'Agent',
  `deliver_to` varchar(80) DEFAULT 'Customer Name',
  `notify_email` varchar(80) DEFAULT 'Customer Email',
  `phone` varchar(80) DEFAULT 'Customer Phone',
  `address` varchar(80) DEFAULT 'Customer Address',
  `vehicle_name` varchar(80) DEFAULT 'Agent Name',
  `organization_id` int(11) NOT NULL,
  `status` varchar(80) DEFAULT 'Status',
  `job_type` varchar(80) DEFAULT 'Job Type',
  `sheet_id` varchar(80) DEFAULT 'Work Order',
  `job_date` varchar(80) DEFAULT 'Job Date',
  `payment_type` varchar(80) DEFAULT 'Payment Type',
  `payment_amount` varchar(80) DEFAULT 'Payment Amount',
  `collected_amount` varchar(100) DEFAULT 'Collected Amount',
  `zone_name` varchar(80) DEFAULT 'Zone',
  `reference_no` varchar(80) DEFAULT 'Reference No',
  `order_type` varchar(80) DEFAULT 'Order Type',
  `latitude` varchar(80) DEFAULT 'Latitude',
  `longitude` varchar(80) DEFAULT 'Longitude',
  `address_2` varchar(80) DEFAULT 'Address Two',
  `province` varchar(80) DEFAULT 'Province',
  `city` varchar(80) DEFAULT 'City',
  `district` varchar(80) DEFAULT 'District',
  `subdistrict` varchar(80) DEFAULT 'Subdistrict',
  `postalcode` varchar(80) DEFAULT 'Postal Code',
  `country` varchar(80) DEFAULT 'Country',
  `order_note` varchar(80) DEFAULT 'Order Note',
  `item_qty` varchar(50) DEFAULT 'Item qty',
  `item_sku` varchar(50) DEFAULT 'Item sku',
  `item_description` varchar(50) DEFAULT 'Item description',
  `unit_price` varchar(100) DEFAULT 'Unit price',
  `relation_recipient` varchar(100) DEFAULT 'Relation Recipient',
  `note` varchar(100) DEFAULT 'Note',
  `instruction` varchar(100) DEFAULT 'Instruction',
  `client_name` varchar(40) DEFAULT 'Client',
  `free_text_1` varchar(80) DEFAULT 'Free Text 1',
  `free_text_2` varchar(80) DEFAULT 'Free Text 2',
  `free_text_3` varchar(80) DEFAULT 'Free Text 3',
  `free_text_4` varchar(80) DEFAULT 'Free Text 4',
  `free_text_5` varchar(80) DEFAULT 'Free Text 5',
  `free_text_6` varchar(80) DEFAULT 'Free Text 6',
  `free_text_7` varchar(80) DEFAULT 'Free Text 7',
  `free_text_8` varchar(80) DEFAULT 'Free Text 8',
  `free_text_9` varchar(80) DEFAULT 'Free Text 9',
  `free_text_10` varchar(80) DEFAULT 'Free Text 10',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file_aliases`
--

INSERT INTO `file_aliases` (`id`, `external_id`, `job_number`, `hub_name`, `assign_to`, `deliver_to`, `notify_email`, `phone`, `address`, `vehicle_name`, `organization_id`, `status`, `job_type`, `sheet_id`, `job_date`, `payment_type`, `payment_amount`, `collected_amount`, `zone_name`, `reference_no`, `order_type`, `latitude`, `longitude`, `address_2`, `province`, `city`, `district`, `subdistrict`, `postalcode`, `country`, `order_note`, `item_qty`, `item_sku`, `item_description`, `unit_price`, `relation_recipient`, `note`, `instruction`, `client_name`, `free_text_1`, `free_text_2`, `free_text_3`, `free_text_4`, `free_text_5`, `free_text_6`, `free_text_7`, `free_text_8`, `free_text_9`, `free_text_10`, `created_at`, `updated_at`) VALUES
(1, 'External ID', 'Order No', 'Cabang', 'Agent', 'Kastemer', 'Email Kastemer', 'Telepon Kastemer', 'Alamat Kastemer', 'Agent Name', 1, 'Status', 'Tipe Pekerjaan', 'Work Order', 'Tanggal2', 'Payment Type', 'Jumlah Duit', 'Uang Terkumpul', 'Zone', 'Reference No', 'Tipe Order', 'Latitude', 'Longitude', 'Alamat Dua', 'Propinsi', 'Kota', 'Kecamatan', 'Kelurahan', 'Kode Pos', 'Negara', 'Catatan Pemesanan', 'qty', 'sku', 'description', 'price', 'Hubungan Penerima', 'Note', 'Instruksi', 'Client', 'Free Text 1', 'Free Text 2', 'Free Text 3', 'Free Text 4', 'Free Text 5', 'Free Text 6', 'Free Text 7', 'Free Text 8', 'Free Text 9', 'Free Text 10', '2019-07-12 00:00:00', '2019-07-25 07:25:44');

-- --------------------------------------------------------

--
-- Table structure for table `kelurahans`
--

CREATE TABLE `kelurahans` (
  `id` int(11) NOT NULL,
  `kode_provinsi` varchar(150) DEFAULT NULL,
  `nama_provinsi` varchar(150) DEFAULT NULL,
  `kode_kota` varchar(150) DEFAULT NULL,
  `nama_kota` varchar(150) DEFAULT NULL,
  `kode_kecamatan` varchar(150) DEFAULT NULL,
  `nama_kecamatan` varchar(150) DEFAULT NULL,
  `kode_kelurahan` varchar(150) DEFAULT NULL,
  `nama_kelurahan` varchar(150) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahans`
--

INSERT INTO `kelurahans` (`id`, `kode_provinsi`, `nama_provinsi`, `kode_kota`, `nama_kota`, `kode_kecamatan`, `nama_kecamatan`, `kode_kelurahan`, `nama_kelurahan`, `created_at`, `updated_at`) VALUES
(1, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010001', 'Cipedak', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(2, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172020', 'Ciracas', '3172020001', 'Cibubur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(3, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030001', 'Pondok Ranggon', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(4, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030003', 'Munjul', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(5, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172010', 'Pasar Rebo', '3172010001', 'Pekayon', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(6, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010002', 'Srengseng Sawah', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(7, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172010', 'Pasar Rebo', '3172010002', 'Kalisari', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(8, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172020', 'Ciracas', '3172020002', 'Kelapa Dua Wetan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(9, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010003', 'Ciganjur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(10, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030004', 'Cipayung', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(11, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030002', 'Cilangkap', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(12, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172010', 'Pasar Rebo', '3172010003', 'Baru', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(13, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172020', 'Ciracas', '3172020003', 'Ciracas', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(14, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172010', 'Pasar Rebo', '3172010004', 'Cijantung', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(15, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010004', 'Jagakarsa', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(16, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030006', 'Bambu Apus', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(17, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172020', 'Ciracas', '3172020004', 'Susukan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(18, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030007', 'Ceger', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(19, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010005', 'Lenteng Agung', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(20, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171030', 'Cilandak', '3171030002', 'Pondok Labu', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(21, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030005', 'Setu', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(22, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020003', 'Kebagusan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(23, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171010', 'Jagakarsa', '3171010006', 'Tanjung Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(24, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172010', 'Pasar Rebo', '3172010005', 'Gedong', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(25, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171030', 'Cilandak', '3171030001', 'Lebak Bulus', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(26, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172020', 'Ciracas', '3172020005', 'Rambutan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(27, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050004', 'Dukuh', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(28, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172030', 'Cipayung', '3172030008', 'Lubang Buaya', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(29, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020005', 'Jati Padang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(30, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050003', 'Kampung Tengah', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(31, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171030', 'Cilandak', '3171030003', 'Cilandak Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(32, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172040', 'Makasar', '3172040001', 'Pinang Ranti', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(33, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020004', 'Pasar Minggu', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(34, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020002', 'Ragunan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(35, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020001', 'Cilandak Timur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(36, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050002', 'Batu Ampar', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(37, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050001', 'Bale Kambang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(38, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171030', 'Cilandak', '3171030005', 'Cipete Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(39, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020006', 'Pejaten Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(40, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172040', 'Makasar', '3172040002', 'Makasar', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(41, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050005', 'Kramat Jati', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(42, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171030', 'Cilandak', '3171030004', 'Gandaria Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(43, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171020', 'Pasar Minggu', '3171020007', 'Pejaten Timur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(44, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050001', 'Pondok Pinang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(45, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080001', 'Kalibata', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(46, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060002', 'Cipete Utara', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(47, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171040', 'Pesanggrahan', '3171040001', 'Bintaro', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(48, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050006', 'Cililitan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(49, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171070', 'Mampang Prapatan', '3171070001', 'Bangka', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(50, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080002', 'Rawa Jati', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(51, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080003', 'Duren Tiga', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(52, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060001', 'Gandaria Utara', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(53, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171040', 'Pesanggrahan', '3171040002', 'Pesanggrahan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(54, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171070', 'Mampang Prapatan', '3171070003', 'Tegal Parang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(55, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060003', 'Pulo', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(56, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080005', 'Pengadegan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(57, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050002', 'Kebayoran Lama Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(58, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172050', 'Kramat Jati', '3172050007', 'Cawang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(59, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080006', 'Cikoko', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(60, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172040', 'Makasar', '3172040004', 'Halim Perdana Kusuma', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(61, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171080', 'Pancoran', '3171080004', 'Pancoran', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(62, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172040', 'Makasar', '3172040003', 'Kebon Pala', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(63, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171070', 'Mampang Prapatan', '3171070004', 'Mampang Prapatan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(64, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172040', 'Makasar', '3172040005', 'Cipinang Melayu', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(65, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060006', 'Kramat Pela', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(66, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060005', 'Melawai', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(67, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171070', 'Mampang Prapatan', '3171070002', 'Pela Mampang', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(68, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060004', 'Petogogan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(69, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050003', 'Kebayoran Lama Utara', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(70, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060009', 'Rawa Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(71, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050004', 'Cipulir', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(72, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060008', 'Selong', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(73, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060007', 'Gunung', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(74, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171040', 'Pesanggrahan', '3171040004', 'Petukangan Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(75, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070003', 'Pondok Kelapa', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(76, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060002', 'Cipinang Cempedak', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(77, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100002', 'Kuningan Timur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(78, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171070', 'Mampang Prapatan', '3171070005', 'Kuningan Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(79, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090004', 'Kebon Baru', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(80, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060001', 'Bidara Cina', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(81, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090003', 'Tebet Timur', '2021-01-26 06:42:46', '2021-01-26 07:53:31'),
(82, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090002', 'Tebet Barat', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(83, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050005', 'Grogol Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(84, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070001', 'Pondok Bambu', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(85, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171040', 'Pesanggrahan', '3171040005', 'Petukangan Utara', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(86, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171040', 'Pesanggrahan', '3171040003', 'Ulujami', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(87, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090001', 'Menteng Dalam', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(88, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070002', 'Duren Sawit', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(89, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171060', 'Kebayoran Baru', '3171060010', 'Senayan', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(90, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060003', 'Cipinang Besar Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(91, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070004', 'Pondok Kopi', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(92, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070005', 'Malaka Jaya', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(93, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020001', 'Sukabumi Selatan', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(94, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070006', 'Malaka Sari', '2021-01-26 06:42:46', '2021-01-26 07:53:32'),
(95, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090005', 'Bukit Duri', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(96, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100003', 'Karet Kuningan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(97, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090006', 'Manggarai Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(98, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100001', 'Karet Semanggi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(99, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060005', 'Cipinang Besar Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(100, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172070', 'Duren Sawit', '3172070007', 'Klender', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(101, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060004', 'Cipinang Muara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(102, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100005', 'Menteng Atas', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(103, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060006', 'Rawa Bunga', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(104, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060008', 'Kampung Melayu', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(105, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010001', 'Joglo', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(106, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172060', 'Jatinegara', '3172060007', 'Balimester', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(107, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100004', 'Karet', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(108, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100003', 'Pisangan Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(109, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171090', 'Tebet', '3171090007', 'Manggarai', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(110, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171050', 'Kebayoran Lama', '3171050006', 'Grogol Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(111, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100006', 'Pasar Manggis', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(112, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100007', 'Guntur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(113, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3171', 'Jakarta Selatan', '3171100', 'Setiabudi', '3171100008', 'Setia budi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(114, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020002', 'Sukabumi Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(115, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020003', 'Kelapa Dua', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(116, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090001', 'Pisangan Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(117, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090002', 'Cipinang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(118, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010001', 'Gelora', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(119, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010003', 'Karet Tengsin', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(120, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100001', 'Kebon Manggis', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(121, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010002', 'Bendungan Hilir', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(122, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100005', 'Utan Kayu Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(123, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010003', 'Meruya Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(124, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010002', 'Srengseng', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(125, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100002', 'Palmeriam', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(126, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173020', 'Menteng', '3173020002', 'Pegangsaan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(127, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100004', 'Kayu Manis', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(128, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173020', 'Menteng', '3173020001', 'Menteng', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(129, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030001', 'Pal Merah', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(130, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080003', 'Pulo Gebang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(131, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172100', 'Matraman', '3172100006', 'Utan Kayu Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(132, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080001', 'Jatinegara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(133, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010004', 'Meruya Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(134, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030002', 'Slipi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(135, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010004', 'Kebon Melati', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(136, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010005', 'Petamburan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(137, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090005', 'Rawamangun', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(138, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030002', 'Paseban', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(139, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173020', 'Menteng', '3173020004', 'Gondangdia', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(140, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010006', 'Kebon Kacang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(141, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090004', 'Jati', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(142, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090003', 'Jatinegara Kaum', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(143, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080002', 'Penggilingan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(144, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173020', 'Menteng', '3173020003', 'Cikini', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(145, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173050', 'Cempaka Putih', '3173050001', 'Rawasari', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(146, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020004', 'Kebon Jeruk', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(147, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030001', 'Kenari', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(148, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030005', 'Kota Bambu Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(149, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173020', 'Menteng', '3173020005', 'Kebon Sirih', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(150, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173010', 'Tanah Abang', '3173010007', 'Kampung Bali', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(151, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173040', 'Johar Baru', '3173040001', 'Johar Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(152, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030003', 'Kemanggisan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(153, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030004', 'Kotabambu Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(154, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030004', 'Kwitang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(155, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173040', 'Johar Baru', '3173040002', 'Kampung Rawa', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(156, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090007', 'Pulo Gadung', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(157, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030003', 'Kramat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(158, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173040', 'Johar Baru', '3173040003', 'Tanah Tinggi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(159, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173040', 'Johar Baru', '3173040004', 'Galur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(160, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040002', 'Tanjung Duren Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(161, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020006', 'Kedoya Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(162, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174030', 'Palmerah', '3174030006', 'Jati Pulo', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(163, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080004', 'Ujung Menteng', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(164, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010005', 'Kembangan Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(165, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173050', 'Cempaka Putih', '3173050003', 'Cempaka Putih Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(166, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030005', 'Senen', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(167, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173030', 'Senen', '3173030006', 'Bungur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(168, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080002', 'Petojo Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(169, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080003', 'Gambir', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(170, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172090', 'Pulo Gadung', '3172090006', 'Kayu Putih', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(171, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173050', 'Cempaka Putih', '3173050002', 'Cempaka Putih Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(172, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040003', 'Tomang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(173, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080001', 'Cideng', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(174, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060001', 'Harapan Mulya', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(175, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040001', 'Tanjung Duren Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(176, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020005', 'Duri Kepa', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(177, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080007', 'Rawa Terate', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(178, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060002', 'Cempaka Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(179, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060007', 'Kemayoran', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(180, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060005', 'Utan Panjang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(181, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080006', 'Duri Pulo', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(182, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080005', 'Petojo Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(183, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173080', 'Gambir', '3173080004', 'Kebon Kelapa', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(184, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174020', 'Kebon Jeruk', '3174020007', 'Kedoya Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(185, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174010', 'Kembangan', '3174010006', 'Kembangan Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(186, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060003', 'Sumur Batu', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(187, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173070', 'Sawah Besar', '3173070001', 'Pasar Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(188, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050002', 'Duri Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(189, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174080', 'Kalideres', '3174080001', 'Semanan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(190, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050001', 'Kali Anyar', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(191, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070002', 'Rawa Buaya', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(192, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080005', 'Cakung Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(193, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060004', 'Serdang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(194, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040004', 'Grogol', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(195, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070001', 'Duri Kosambi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(196, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175050', 'Kelapa Gading', '3175050002', 'Kelapa Gading Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(197, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060001', 'Krukut', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(198, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040005', 'Jelambar', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(199, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060006', 'Kebon Kosong', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(200, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050004', 'Duri Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(201, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3172', 'Jakarta Timur', '3172080', 'Cakung', '3172080006', 'Cakung Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(202, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173060', 'Kemayoran', '3173060008', 'Gunung Sahari Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(203, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060002', 'Maphar', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(204, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060003', 'Taman Sari', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(205, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050006', 'Jembatan Besi', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(206, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060006', 'Keagungan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(207, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173070', 'Sawah Besar', '3173070004', 'Karang Anyar', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(208, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173070', 'Sawah Besar', '3173070003', 'Kartini', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(209, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050005', 'Krendang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(210, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050003', 'Tanah Sereal', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(211, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040006', 'Wijaya Kusuma', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(212, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174040', 'Grogol Petamburan', '3174040007', 'Jelambar Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(213, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060004', 'Tangki', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(214, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050007', 'Angke', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(215, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050008', 'Jembatan Lima', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(216, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173070', 'Sawah Besar', '3173070002', 'Gunung Sahari Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(217, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050009', 'Tambora', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(218, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070003', 'Kedaung Kali Angke', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(219, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060007', 'Glodok', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(220, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060005', 'Mangga Besar', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(221, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030002', 'Sunter Jaya', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(222, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175050', 'Kelapa Gading', '3175050003', 'Pegangsaan Dua', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(223, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175050', 'Kelapa Gading', '3175050001', 'Kelapa Gading Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(224, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060001', 'Sukapura', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(225, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3173', 'Jakarta Pusat', '3173070', 'Sawah Besar', '3173070005', 'Mangga Dua Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(226, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050011', 'Pekojan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(227, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174050', 'Tambora', '3174050010', 'Roa Malaka', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(228, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060002', 'Rorotan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(229, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174060', 'Taman Sari', '3174060008', 'Pinangsia', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(230, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070005', 'Cengkareng Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(231, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174080', 'Kali deres', '3174080002', 'Kali Deres', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(232, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175020', 'Pademangan', '3175020001', 'Pademangan Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(233, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175020', 'Pademangan', '3175020002', 'Pademangan Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(234, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070004', 'Kapuk', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(235, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040002', 'Tugu Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(236, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030001', 'Sunter Agung', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(237, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040001', 'Rawa Badak Selatan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(238, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030003', 'Papanggo', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(239, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174070', 'Cengkareng', '3174070006', 'Cengkareng Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(240, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175010', 'Penjaringan', '3175010003', 'Pejagalan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(241, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040003', 'Tugu Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(242, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174080', 'Kalideres', '3174080003', 'Pegadungan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(243, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030004', 'Warakas', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(244, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040007', 'Rawa Badak Utara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(245, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030006', 'Kebon Bawang', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(246, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030005', 'Sungai Bambu', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(247, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040004', 'Lagoa', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(248, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060006', 'Semper Barat', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(249, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060005', 'Semper Timur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(250, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175010', 'Penjaringan', '3175010002', 'Kapuk Muara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(251, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175020', 'Pademangan', '3175020003', 'Ancol', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(252, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174080', 'Kalideres', '3174080004', 'Tegal Alur', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(253, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175040', 'Koja', '3175040006', 'Koja', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(254, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060004', 'Cilincing', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(255, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060007', 'Kali Baru', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(256, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3174', 'Jakarta Barat', '3174080', 'Kalideres', '3174080005', 'Kamal', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(257, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175010', 'Penjaringan', '3175010004', 'Penjaringan', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(258, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175030', 'Tanjung Priok', '3175030007', 'Tanjung Priok', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(259, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175010', 'Penjaringan', '3175010005', 'Pluit', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(260, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175010', 'Penjaringan', '3175010001', 'Kamal Muara', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(261, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3175', 'Jakarta Utara', '3175060', 'Cilincing', '3175060003', 'Marunda', '2021-01-26 06:42:47', '2021-01-26 07:53:32'),
(262, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101020', 'KEPULAUAN SERIBU UTARA', '3101020003', 'PULAU HARAPAN', '2021-01-26 06:42:47', '2021-01-26 07:53:33'),
(263, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101020', 'KEPULAUAN SERIBU UTARA', '3101020002', 'PULAU KELAPA', '2021-01-26 06:43:12', '2021-01-26 07:53:33'),
(264, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101020', 'KEPULAUAN SERIBU UTARA', '3101020001', 'PULAU PANGGANG', '2021-01-26 06:43:12', '2021-01-26 07:53:33'),
(265, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101010', 'KEPULAUAN SERIBU SELATAN', '3101010002', 'PULAU PARI', '2021-01-26 06:43:12', '2021-01-26 07:53:33'),
(266, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101010', 'KEPULAUAN SERIBU SELATAN', '3101010001', 'PULAU TIDUNG', '2021-01-26 06:43:12', '2021-01-26 07:53:33'),
(267, '31', 'DAERAH KHUSUS IBUKOTA JAKARTA', '3101', 'KEPULAUAN SERIBU', '3101010', 'KEPULAUAN SERIBU SELATAN', '3101010003', 'PULAU UNTUNG JAWA', '2021-01-26 06:43:12', '2021-01-26 07:53:33');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `dashboard` varchar(100) DEFAULT 'Dashboard',
  `zone` varchar(100) DEFAULT 'Zone',
  `hub` varchar(100) DEFAULT 'Hub',
  `vehicle` varchar(100) DEFAULT 'Vehicle',
  `map` varchar(50) DEFAULT 'Map',
  `job` varchar(100) DEFAULT 'Order',
  `item` varchar(100) DEFAULT 'Item',
  `task` varchar(100) NOT NULL DEFAULT 'Job',
  `manual_pod` varchar(100) DEFAULT 'Manual POD',
  `failed_reason` varchar(100) DEFAULT 'Failed Reason',
  `organization_setting` varchar(100) DEFAULT 'Organization Setting',
  `summary` varchar(50) DEFAULT 'Summary',
  `client` varchar(30) DEFAULT 'Client',
  `organization_id` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `dashboard`, `zone`, `hub`, `vehicle`, `map`, `job`, `item`, `task`, `manual_pod`, `failed_reason`, `organization_setting`, `summary`, `client`, `organization_id`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'Zona', 'Cabang', 'Kurir', 'Map', 'Order', 'Barang', 'Job', 'Manual POD', 'Alasan Gagal', 'Organization Setting', 'Summary', 'Partner', '1', '2019-07-09 00:00:00', '2019-08-13 04:14:43');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL,
  `generated_id` varchar(10) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  `success_photo` int(1) DEFAULT '1',
  `success_sign` int(1) DEFAULT '1',
  `failed_photo` int(1) DEFAULT '1',
  `failed_sign` int(1) DEFAULT '1',
  `currency` varchar(10) DEFAULT 'Rp',
  `timezone` varchar(10) DEFAULT '+7',
  `language` varchar(40) DEFAULT NULL,
  `slug` varchar(40) DEFAULT NULL,
  `api-key` varchar(40) DEFAULT NULL,
  `phone_code` varchar(10) DEFAULT NULL,
  `latitude` varchar(70) DEFAULT '0.651320',
  `longitude` varchar(70) DEFAULT '114.102574',
  `zoom` int(3) DEFAULT '5',
  `warning_interval` int(5) DEFAULT '15',
  `image` varchar(200) DEFAULT NULL,
  `favicon` varchar(200) DEFAULT NULL,
  `sm_url` varchar(300) DEFAULT NULL,
  `sm_api_key` varchar(50) DEFAULT NULL,
  `sm_api_name` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `generated_id`, `name`, `success_photo`, `success_sign`, `failed_photo`, `failed_sign`, `currency`, `timezone`, `language`, `slug`, `api-key`, `phone_code`, `latitude`, `longitude`, `zoom`, `warning_interval`, `image`, `favicon`, `sm_url`, `sm_api_key`, `sm_api_name`, `created_at`, `updated_at`) VALUES
(1, 'WkSwZFB5Hk', 'JSC', 0, 0, 1, 1, '$', '+7', 'ina', NULL, 'api-key-GDAE$!@FASDA', '+62', '0.3515602939922709', '119.26757812500001', 5, 10, 'jsc_logo.png', '2019-08-12_07_05_favicon.ico', 'https://www.google.com', 'nqStGQK6fneKjT.DeL.6jeEii8E6giXMzp3FP0JYgLbkZMqp4e', NULL, '2019-04-23 04:27:24', '2020-01-10 06:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `rumah_sakits`
--

CREATE TABLE `rumah_sakits` (
  `id` int(11) NOT NULL,
  `id_rsu` int(5) NOT NULL,
  `nama_rsu` varchar(250) DEFAULT NULL,
  `jenis_rsu` varchar(50) DEFAULT NULL,
  `alamat` text,
  `latitude_object` varchar(50) DEFAULT NULL,
  `longitude_object` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(15) DEFAULT NULL,
  `telepon` varchar(100) DEFAULT NULL,
  `faximile` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `kode_kota` varchar(100) DEFAULT NULL,
  `kode_kecamatan` varchar(100) DEFAULT NULL,
  `kode_kelurahan` varchar(100) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rumah_sakits`
--

INSERT INTO `rumah_sakits` (`id`, `id_rsu`, `nama_rsu`, `jenis_rsu`, `alamat`, `latitude_object`, `longitude_object`, `kode_pos`, `telepon`, `faximile`, `website`, `email`, `kode_kota`, `kode_kecamatan`, `kode_kelurahan`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 2, 'Tarakan', 'Rumah Sakit Umum Daerah', 'Jl. Kyai Caringin No. 7', '-6.171333', '106.810013', '10150', '3503150|3503003|3508993', '3503412| 3863309', 'www.rstarakanjakarta.com', 'kusmedi@gmail.com, rsd_tarakan@yahoo.com', '3173', '3173080', '3173080001', '-6.171333', '106.810013', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(2, 3, 'DR. Mintohardjo', 'Rumah Sakit Umum TNI - Angkatan Laut', 'Jl. Bendungan Hilir No. 17', '-6.210848', '106.811996', '10210', '5703081 - 85', '5711997', 'www.rsaldrmintohardjo.com', 'rsalmintohardjo@ymail.com', '3173', '3173010', '3173010002', '-6.210848', '106.811996', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(3, 4, 'Sahid Sahirman', 'Rumah Sakit Umum', 'Jl. Jenderal Sudirman Kavling 86', '-6.209445', '106.819382', '10220', '5704591|5703231', '5705505|5727213', 'www.ssmh.co.id', 'information@ssmh.co.id', '3173', '3173010', '3173010003', '-6.209445', '106.819382', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(4, 5, 'PGI Cikini', 'Rumah Sakit Umum', 'Jl. Raden Saleh No. 40 ', '-6.19127', '106.841408', '10330', '38997777', '31924663|31908391', ' www.rscikini.com', 'tedjowa@yahoo.com, mail@rscikini.com', '3173', '3173020', '3173020003', '-6.19127', '106.841408', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(5, 6, 'Menteng Mitra Afia', 'Rumah Sakit Umum', 'Jl. Kali Pasir  No. 9', '-6.187353', '106.839081', '10340', '3154050', '3146309', 'www.rsmentengmitraafia.com', 'panela_ramadita@yahoo.com, info@rsmentengmitraafia.com', '3173', '3173020', '3173020003', '-6.187353', '106.839081', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(6, 7, 'Abdi Waluyo', 'Rumah Sakit Umum', 'Jalan  HOS Cokroaminoto  Nomor 31-33', '-6.189762', '106.8293', '10350', '3144989', '31930866', NULL, 'dr.migot@yahoo.co.id', '3173', '3173020', '3173020004', '-6.189762', '106.8293', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(7, 8, 'Bunda Jakarta', 'Rumah Sakit Umum', 'Jl. Teuku Cik Ditiro No. 21', '-6.196026', '106.836517', '10350', '3909692|31923939', '3101077', 'www.bunda.co.id', 'bundahospital@bunda.co.id', '3173', '3173020', '3173020004', '-6.196026', '106.836517', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(8, 9, 'Gatot Soebroto', 'Rumah Sakit Umum Pusat Angkatan Darat', 'Jl. Dr. Abdul Rachman Saleh 24', '-6.176608', '106.836861', '10410', '3441008', '3440693', NULL, 'rspadgatsu@pdpersi.co.id', '3173', '3173030', '3173030005', '-6.176608', '106.836861', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(9, 10, 'Dr. Cipto Mangunkusumo', 'Rumah Sakit Umum Pusat Nasional', 'Jl. Diponegoro No. 71', '-6.196762', '106.846886', '10430', '3918301', '3148991', 'www.rscm.co.id', 'perbendaharaan.rscm@yahoo.com, dir_rscm@yahoo.com', '3173', '3173030', '3173030001', '-6.196762', '106.846886', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(10, 11, 'Moh Ridwan Meuraksa', 'Rumah Sakit Tk. II', 'Jl. Kramat Raya  No. 174', '-6.191414', '106.847', '10430', '3150535 |323094', '3916888', NULL, 'tanjung_widia@yahoo.com, rsmrm74@yahoo.com', '3173', '3173030', '3173030001', '-6.191414', '106.847', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(11, 12, 'Kramat 128', 'Rumah Sakit Umum', 'Jalan  Kramat Raya  Nomor 128', '-6.18536', '106.843994', '10430', '3909513 ; 3909514', '3909125', 'www.rskramat128.com', 'dyah.agustina.w@gmail.com ; info@rskramat128.com', '3173', '3173030', '3173030001', '-6.18536', '106.843994', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(12, 13, 'PK. St. Carolus ', 'Rumah Sakit Umum', 'Jl. Salemba Raya No. 41', '-6.195917', '106.851006', '10440', '3904441', '3103226', NULL, 'markus_wasesa@yahoo.com, humas_pksc.or.id, carolus@pdpersi.co.id', '3173', '3173030', '3173030002', '-6.195917', '106.851006', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(13, 14, 'Moh. Husni Thamrin Salemba', 'Rumah Sakit Umum', 'Jl. Salemba Tengah 26 - 28', '-6.193444', '106.852005', '10440', '3904422 Ext.1900 ;1919', '3107816-2305182', 'www.thamrinhospital.com', 'info@thamrinhospital.com', '3173', '3173030', '3173030002', '-6.193444', '106.852005', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(14, 15, 'Islam Jakarta Cempaka Putih', 'Rumah Sakit Umum', 'Jl. Cempaka Putih Tengah I / 1', '-6.170203', '106.870659', '10510', '42801567-4250451', '4206681', 'www.rsi.co.id', 'rsijpusat@rsi.co.id,  rsij@cbn.net.id', '3173', '3173050', '3173050002', '-6.170203', '106.870659', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(15, 16, 'Pertamina Jaya', 'Rumah Sakit Umum', 'Jl. Achmad Yani No. 2, By Pass', '-6.173366', '106.876312', '10510', '4211911', '4211913', 'www.rspj.co.id', 'pertamedika.rspj@gmail.com', '3173', '3173050', '3173050002', '-6.173366', '106.876312', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(16, 17, 'Evasari', 'Rumah Sakit Umum', 'Jalan  Rawamangun  Nomor 47', '-6.193554', '106.859947', '10570', '4202851-4', '4209725', 'www.evasari-jakarta.awalbros.com', 'info@rsia_evasari@yahoo.com', '3173', '3173050', '3173050001', '-6.193554', '106.859947', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(17, 18, 'Mitra Kemayoran', 'Rumah Sakit Umum', 'Jl. Landas Pacu Timur', '-6.151878', '106.858414', '10630', '6545555', '6545959', 'www.mitrakemayoran.com', 'kemayoran@mitrakeluarga.com', '3175', '3175030', '3175030002', '-6.151878', '106.858414', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(18, 19, 'Husada ', 'Rumah Sakit Umum', 'Jl. Raya Mangga Besar Raya 137 - 139', '-6.147598', '106.828812', '10730', '6260208 - 6490090', '6497494', 'www.husada.co.id', 'husada@husada.co.id', '3173', '3173070', '3173070005', '-6.147598', '106.828812', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(19, 21, 'Islam Jakarta Sukapura', 'Rumah Sakit Umum', 'Jl. Tipar - Cakung No. 5', '-6.139646', '106.920921', '14140', '4400778|4400779|4400781', '4400782', 'www.rsijsukapura@.co.id', 'rsijsukapura@gmail.com', '3175', '3175060', '3175060001', '-6.139646', '106.920921', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(20, 22, 'Pekerja', 'Rumah Sakit Umum', 'Jalan  Raya Cakung Cilincing, RT.002/RW.003 ', '-6.144747', '106.923759', '14140', '29484848  (10 lines)', '29482875', 'www.kbn.co.id', 'rsu.pekerja@gmail.com', '3175', '3175060', '3175060001', '-6.144747', '106.923759', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(21, 23, 'Koja', 'Rumah Sakit Umum Daerah', 'Jl. Deli No. 4  Tanjung Priok', '-6.108805', '106.900162', '14220', '43938478', '4352401 - 4372273', 'www.rsudkoja.com', 'rsudkoja@ymail.com', '3175', '3175040', '3175040006', '-6.108805', '106.900162', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(22, 24, 'Mitra Keluarga Kelapa Gading', 'Rumah Sakit Umum', 'Jl. Bukit Gading Raya Kav. II', '-6.151771', '106.897102', '14240', '45852700', '45852727', 'www.mitrakeluarga.com', 'kelapagading@mitrakeluarga.com', '3175', '3175050', '3175050001', '-6.151771', '106.897102', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(23, 25, 'Gading Pluit', 'Rumah Sakit Umum', 'Jl. Boulevard Timur Raya RT. 006 / 02', '-6.166122', '106.915993', '14250', '4521001|4520201', '4520578', NULL, 'gadingpluit@gadingpluit-hospital.com', '3175', '3175050', '3175050003', '-6.166122', '106.915993', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(24, 26, 'Pelabuhan Jakarta', 'Rumah Sakit Umum', 'Jl. Kramat Jaya, Tanjung Priok', '-6.124692', '106.917519', '14260', '4403026', '4403551|4406886', 'www.rspelabuhan.com', 'marketing.rspj@gmail.com', '3175', '3175040', '3175040003', '-6.124692', '106.917519', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(25, 27, 'Mulyasari', 'Rumah Sakit Umum', 'Jl. Raya Plumpang Semper No. 19', '-6.129088', '106.906509', '14260', '4390666|4393111|4393888', '43935676', 'www.rsmulyasari.com', 'info@rsmulyasari.com', '3175', '3175040', '3175040003', '-6.129088', '106.906509', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(26, 28, 'Port Medical Center', 'Rumah Sakit Umum', 'Jl. Enggano No. 10', '-6.110295', '106.889389', '14310', '43902350', '43902302', NULL, 'tienastari_jpkm@yahoo.com', '3175', '3175030', '3175030007', '-6.110295', '106.889389', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(27, 29, 'Sukmul Sisma Medika', 'Rumah Sakit Umum', 'Jl. Tawes No. 18-20 ', '-6.112143', '106.881996', '14310', '4301269', '4301272', NULL, 'sriha36@gmail.com, sukmul@indosat.net.id', '3175', '3175030', '3175030007', '-6.112143', '106.881996', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(28, 30, 'Puri Medika', 'Rumah Sakit Umum', 'Jl Sungai Bambu  No. 5', '-6.129145', '106.890198', '14330', '43903355-43901273', '43903388', 'www.purimdika.com', 'purmed-medcen@centrin.net.id', '3175', '3175030', '3175030006', '-6.129145', '106.890198', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(29, 31, 'Satya Negara', 'Rumah Sakit Umum', 'Jl. Agung Utara Raya Blok A No. 1', '-6.138887', '106.861549', '14350', '65836583|64715200|64715900', '687813-64717813', 'www.rssatyanegara.com', 'chitra_usmanuddin@yahoo.com, rssn@cbn.net.id', '3175', '3175030', '3175030001', '-6.138887', '106.861549', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(30, 32, 'Royal Progress', 'Rumah Sakit Umum', 'Jl. Danau Sunter Utara Blok F 25 No. 12', '-6.139086', '106.865395', '14350', '6459877-6400261', '6400778', 'www.royalprogress.com', 'setdir.rssn@gmail.com, ask.us@royalprogress.com', '3175', '3175030', '3175030001', '-6.139086', '106.865395', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(31, 33, 'Hermina Podomoro', 'Rumah Sakit Umum', 'Jalan  Danau Agung 2 Blok E 3  Nomor 28-30', '-6.145772', '106.859978', '14350', '6404910', '6518720', 'www.hermina.com', 'podomoro@rsiahermina.com', '3175', '3175030', '3175030001', '-6.145772', '106.859978', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(32, 34, 'Atma Jaya', 'Rumah Sakit Umum', 'Jl. Pluit Raya No. 2', '-6.126524', '106.792809', '14440', '6606121 ; 6606127', '6606122', NULL, 'rsatmajaya@pdpersi.co.id', '3175', '3175010', '3175010004', '-6.126524', '106.792809', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(33, 35, 'Pluit', 'Rumah Sakit Umum', 'Jl. Raya Pluit Selatan No. 2', '-6.125572', '106.799835', '14440', '6685006 - 6685070', '6684878', 'www.pluit-hospital.com', 'pluithospital@pluit-hospital.com', '3175', '3175010', '3175010004', '-6.125572', '106.799835', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(34, 36, 'Duta Indah', 'Rumah Sakit Umum', 'Jalan  Teluk Gong Raya  Nomor 12', '-6.138351', '106.783676', '14450', '66676188', '666676190', NULL, NULL, '3175', '3175010', '3175010003', '-6.138351', '106.783676', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(35, 37, 'Pantai Indah Kapuk', 'Rumah Sakit Umum', 'Jl. Pantai Indah Utara 3 Sek. Utr. Tmr Blok T', '-6.111786', '106.752617', '14460', '5880911', '5880910', 'www.pikhospital.co.id', 'pr@pikhospital.co.id', '3175', '3175010', '3175010002', '-6.111786', '106.752617', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(36, 39, 'Pelni Petamburan', 'Rumah Sakit Umum', 'Jl. Aip II K. S. Tubun No. 92-94', '-6.193323', '106.804817', '11410', '5480608', '5483145', NULL, 'info@rspelni.co.id, danty@rspelni.net', '3174', '3174030', '3174030002', '-6.193323', '106.804817', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(37, 40, 'Bhakti Mulia', 'Rumah Sakit Umum', 'Jl. Aipda K. S. Tubun No. 79', '-6.199652', '106.800537', '11410', '5481625|5481262', '5331544', NULL, 'rs.bhaktimulia@yahoo.co.id', '3174', '3174030', '3174030002', '-6.199652', '106.800537', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(38, 41, 'Sumber Waras ', 'Rumah Sakit Umum', 'Jl. Kyai Tapa No. 1', '-6.167247', '106.79837', '11440', '5682011-5663726', '5673122', 'www.rssumberwaras.com', 'bambang.heri@gmail.com, sekretariat@rssumberwaras.com', '3174', '3174040', '3174040003', '-6.167247', '106.79837', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(39, 42, 'Royal Taruma', 'Rumah Sakit Umum', 'Jl. Daan Mogot  No. 34', '-6.166489', '106.785637', '11470', '56958338|56967788', '56958589', 'www.rs-royaltaruma.com', 'direksi@rs-royaltaruma.com', '3174', '3174040', '3174040001', '-6.166489', '106.785637', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(40, 43, 'Patria IKKT', 'Rumah Sakit Umum', 'Jalan Cendrawasih Nomor 1 Komp.Dep.Han, Mabes TNI  Slipi', '-6.197132', '106.7929', '11480', '5308981-5308984', '5346387', NULL, 'patria_ikkt@yahoo.co.id', '3174', '3174030', '3174030001', '-6.197132', '106.7929', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(41, 44, 'Puri Mandiri Kedoya', 'Rumah Sakit Umum', 'Jl. Kedoya Raya / Al-Kamal No. 2', '-6.187422', '106.761673', '11520', '5802126 ; 5828299', '5816185 ; 5828499', NULL, 'customer_service@rspurimandirikedoya.com', '3174', '3174020', '3174020006', '-6.187422', '106.761673', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(42, 45, 'Grha Kedoya', 'Rumah Sakit Umum', 'Jl. Panjang Arteri 26', '-6.168242', '106.765175', '11520', '56982222|29910999', '56982233', 'www.grhakedoya.com', 'info@grhakedoya.com', '3174', '3174020', '3174020007', '-6.168242', '106.765175', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(43, 46, 'Siloam Hospitals Kebon Jeruk', 'Rumah Sakit Umum', 'Jl. Raya Pejuangan Kav. 8', '-6.190661', '106.76384', '11530', '5300887 ; 8 ; 9', '5321766', 'www.siloamhospital.com', 'info@siloamhospitals.com', '3174', '3174020', '3174020004', '-6.190661', '106.76384', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(44, 47, 'Medika Permata Hijau', 'Rumah Sakit Umum', 'Jl. Raya Kebayoran Lama No. 64 ', '-6.218273', '106.77832', '11560', '5347411 - 5305288', '5305291', 'www.rsmph.co.id', 'mph-mkt@rad.net.id', '3174', '3174020', '3174020001', '-6.218273', '106.77832', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(45, 48, 'Pondok Indah - Puri Indah', 'Rumah Sakit Umum', 'Jl. Puri Indah Raya  Blok S-2', '-6.185972', '106.735947', '11610', '25695222|25695200', '25695205', 'www.rspondokindah.co.id', 'rspi@rspondokindah.co.id', '3174', '3174010', '3174010005', '-6.185972', '106.735947', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(46, 49, 'Cengkareng', 'Rumah Sakit Umum Daerah', 'Jl. Kamal Raya, Bumi Cengkareng Indah', '-6.142909', '106.735199', '11730', '5442692 |54372874 - 6', '54372884 ; 54374844 |5442693', 'www.rsudcengkareng.com', 'hafifahany@rsudcengkareng.com', '3174', '3174070', '3174070005', '-6.142909', '106.735199', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(47, 50, 'Hermina Daan Mogot', 'Rumah Sakit Umum', 'Jl. Kintamani Raya No. 2, Kawasan Daan Mogot Baru', '-6.152913', '106.71241', '11840', '5408989 - 5411109', '5449869', 'www.herminahospitalgroup.com', 'daanmogot@herminahospitalgroup.com', '3174', '3174080', '3174080002', '-6.152913', '106.71241', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(48, 52, 'Pusat Pertamina ', 'Rumah Sakit Umum', 'Jl. Kyai Maja No. 43', '-6.240607', '106.792831', '12120', '7219202-7200290', '7209811|7203540', 'www.rspp.co.id', 'dr.maulana@gmail.com, syafik_a@hotmail.com, humasrspp@pertamedika.co.id', '3171', '3171060', '3171060007', '-6.240607', '106.792831', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(49, 53, 'Muhammadiyah Taman Puring', 'Rumah Sakit Umum', 'Jl. Gandaria I / 20', '-6.241654', '106.787193', '12130', '7208358|7250243', '7234823', 'www.rsiamtp.com', 'rsiamtp@rsiamtp.com', '3171', '3171060', '3171060006', '-6.241654', '106.787193', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(50, 54, 'Gandaria', 'Rumah Sakit Umum', 'Jl. Gandaria Tengah II No. 6 - 14', '-6.244061', '106.790398', '12140', '7250718-7203311', '7248175 - 7222038', NULL, NULL, '3171', '3171060', '3171060006', '-6.244061', '106.790398', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(51, 55, 'Yadika Kebayoran Lama', 'Rumah Sakit Umum', 'Jalan  Ciputat Raya  Nomor 5', '-6.255353', '106.777405', '12240', '7291074|7291077', '72895046', NULL, 'rsyadika@yahoo.com', '3171', '3171050', '3171050002', '-6.255353', '106.777405', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(52, 56, 'Pondok Indah - Pondok Indah', 'Rumah Sakit Umum', 'Jl. Metro Duta Kav. UE,  Pondok Indah', '-6.283649', '106.781296', '12310', '7657525-7692252', '7502324', 'www.rspondokindah.co.id', 'rspi@rspondokindah.co.id', '3171', '3171050', '3171050001', '-6.283649', '106.781296', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(53, 57, 'Bhayangkara Sespimma Polri', 'Rumah Sakit Umum', 'Jalan  Ciputat Raya Nomor 40', '-6.286687', '106.770523', '12310', '021-7650384 ; 7666087 ; 7692919', '021-7666106', NULL, 'rs.sespimma_polri@yahoo.co.id, yenifebrianti66@yahoo.com', '3171', '3171050', '3171050001', '-6.286687', '106.770523', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(54, 58, 'Dr. Suyoto', 'Rumah Sakit Umum', 'Jl. R.C. Veteran No. 178', '-6.267451', '106.766129', '12330', '7342581', '77884000|73425817', 'www.suyotohospital.com', 'pemasaranrsds@yahoo.com, humas@suyotohospital.com', '3171', '3171040', '3171040001', '-6.267451', '106.766129', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(55, 59, 'Fatmawati ', 'Rumah Sakit Umum Pusat', 'Jl. RS. Fatmawati', '-6.295009', '106.795578', '12430', '7660552|7501524', '7690123 - 7504022', 'www.fatmawatihospital.com', 'rsupf@fatmawatihospita.com', '3171', '3171030', '3171030003', '-6.295009', '106.795578', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(56, 60, 'Setia Mitra', 'Rumah Sakit Umum', 'Jl. RS Fatmawati No. 80 - 82', '-6.281337', '106.795708', '12430', '7656000', '7656875', NULL, 'dwilaras_p@yahoo.com,  info@rssetiamitra.co.id', '3171', '3171030', '3171030003', '-6.281337', '106.795708', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(57, 61, 'Siloam TB Simatupang', 'Rumah Sakit Umum', 'Jl. RA. Kartini No.8', '-6.292365', '106.783905', '12430', '29531900', '', 'www.siloamhospital.com', 'irna.hardiawan@siloamhospitals.com', '3171', '3171030', '3171030001', '-6.292365', '106.783905', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(58, 62, 'Mayapada', 'Rumah Sakit Umum', 'Jl. Lebak Bulus I  RT.006 / RW.004', '-6.298736', '106.785728', '12440', '', '', NULL, NULL, '3171', '3171030', '3171030003', '-6.298736', '106.785728', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(59, 63, 'Prikasih', 'Rumah Sakit Umum', 'Jl. RS. Fatmawati No. 74  RT.005 / RW.01', '-6.314489', '106.793518', '12450', '7501192|7504669', '7505148', 'www.rsprikasih.com', 'prikasih@cbn.net.id', '3171', '3171030', '3171030002', '-6.314489', '106.793518', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(60, 64, 'Siaga Raya', 'Rumah Sakit Umum', 'Jl. Siaga Raya Kav. 4 - 8', '-6.272883', '106.838951', '12510', '7972750-7972790', '7970494', NULL, 'rs_sigaraya@pdpersi.co', '3171', '3171020', '3171020006', '-6.272883', '106.838951', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(61, 65, 'Rumkital Marinir Cilandak', 'Rumah Sakit Umum', 'Jl. Raya Cilandak  KKO', '-6.305478', '106.813354', '12560', '7805296|7805415', '7812764', NULL, 'enytiar@yahoo.com, layanan@rsmarinir.com', '3171', '3171020', '3171020001', '-6.305478', '106.813354', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(62, 66, 'Ali Sibroh Malisi', 'Rumah Sakit Umum', 'Jalan  Warung Silah  Nomor 1  RT.008 / RW.05', '-6.344991', '106.813705', '12630', '021-7868172|7270681', '78885063', NULL, 'md_azri@yahoo.com', '3171', '3171010', '3171010001', '-6.344991', '106.813705', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(63, 67, 'Zahirah', 'Rumah Sakit Umum', 'Jl. Sirsak No. 21', '-6.335719', '106.823326', '12690', '7872210|78888723', '7270013', 'www.rszahirah.com', 'marketing@rszahirah.com', '3171', '3171010', '3171010004', '-6.335719', '106.823326', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(64, 68, 'Jakarta Medical Center', 'Rumah Sakit Umum', 'Jl. Warung Buncit Raya No. 15', '-6.271507', '106.830147', '12740', '7985177-7980888', '7940838', NULL, 'rs_jmc@cbn.net.id', '3171', '3171080', '3171080001', '-6.271507', '106.830147', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(65, 69, 'Siloam Asri', 'Rumah Sakit Umum', 'Jalan Duren Tiga Raya Nomor 20  RT.004 / RW.01', '-6.254546', '106.83213', '12760', '7992211', '7996505', NULL, NULL, '3171', '3171080', '3171080003', '-6.254546', '106.83213', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(66, 70, 'Tria Dipa', 'Rumah Sakit Umum', 'Jl. Raya Pasar Minggu No. 3 A', '-6.250846', '106.841957', '12780', '7974071-73', '7974074', NULL, 'customer@rs-triadipa.com', '3171', '3171080', '3171080004', '-6.250846', '106.841957', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(67, 71, 'Tebet', 'Rumah Sakit Umum', 'Jl. MT. Haryono No. 8', '-6.242549', '106.849945', '12810', '8307540', '8311760', NULL, NULL, '3171', '3171090', '3171090002', '-6.242549', '106.849945', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(68, 72, 'Jakarta', 'Rumah Sakit Umum', 'Jl. Jend. Sudirman Kav. 49 ', '-6.218441', '106.816101', '12930', '5732241', '5710240 ; 5710249', NULL, NULL, '3171', '3171100', '3171100001', '-6.218441', '106.816101', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(69, 73, 'Metropolitan Medical Center', 'Rumah Sakit Umum', 'Jl. HR. Rasuna Said Kav. C-21 Kuningan', '-6.219806', '106.832146', '12940', '5203435', '5203417', 'www.rsmmc.co.id/mmc@rsmmc.co.id', 'mmc@rsmmc.co.id', '3171', '3171100', '3171100003', '-6.219806', '106.832146', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(70, 74, 'Medistra', 'Rumah Sakit Umum', 'Jl. Jenderal Gatot Subroto Kav. 59', '-6.239865', '106.833328', '12950', '5210200', '5210184', NULL, 'customercare@medistra.com, medistra@cbn.net.id', '3171', '3171100', '3171100002', '-6.239865', '106.833328', '2021-01-26 06:25:51', '2021-01-26 06:25:51'),
(71, 75, 'Agung', 'Rumah Sakit Umum', 'Jl. Sultan Agung No. 67', '-6.209167', '106.846275', '12970', '8295971-8294955', '8305791', NULL, 'rsagung@pdpersi.co.id', '3171', '3171100', '3171100006', '-6.209167', '106.846275', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(72, 77, 'Columbia Asia Pulomas', 'Rumah Sakit Umum', 'Jl. Kayu Putih Raya  No. 1  RT.003 / RW.016', '-6.182741', '106.891495', '13210', '47883195', '47883199', 'www.rsadmira.com', 'rsadmira@rsadmira.com', '3172', '3172090', '3172090007', '-6.182741', '106.891495', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(73, 78, 'Omni Medical Center', 'Rumah Sakit Umum', 'Jl. Pulomas Barat VI No.20', '-6.17582', '106.885246', '13210', '4723332-4722719', '4718081', 'www.omni-hospitals.com', 'normariati70@yahoo.com', '3172', '3172090', '3172090006', '-6.17582', '106.885246', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(74, 79, 'Kartika Pulomas ', 'Rumah Sakit Umum', 'Jalan  Pulomas Timur K. Nomor 2', '-6.17582', '106.885246', '13210', '4723402 - 4703333', '4723402', NULL, NULL, '3172', '3172090', '3172090006', '-6.17582', '106.885246', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(75, 80, 'Aneka Tambang Medika (Antam Medika)', 'Rumah Sakit Umum', 'Jl. Pemuda Raya No. 1A RT.002 / RW.07', '-6.192658', '106.902603', '13210', '29378939|29378943', '29378960', 'www.antammedika.co.id', 'rumahsakit@antammedika.co.id', '3172', '3172090', '3172090003', '-6.192658', '106.902603', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(76, 81, 'Persahabatan ', 'Rumah Sakit Umum Pusat', 'Jl. Persahabatan Raya ', '-6.202596', '106.885071', '13220', '4891708-4890696', '4711222|478608873', 'www.persahabatan.co.id', 'rsp@pcraahabatan.ce.1d', '3172', '3172090', '3172090001', '-6.202596', '106.885071', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(77, 82, 'Dharma Nugraha', 'Rumah Sakit Umum', 'Jl. Balai Pustaka Baru No. 19', '-6.195859', '106.886421', '13220', '4707433-4707437', '4707428', NULL, 'dharmanugraha@yahoo.com', '3172', '3172090', '3172090005', '-6.195859', '106.886421', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(78, 83, 'Mediros', 'Rumah Sakit Umum', 'Jl. Perintis Kemerdekaan Kav. 149', '-6.179724', '106.903748', '13260', '4721336 - 37', '4891937|4891937', 'www.rs-mediros.com', 'lumentagrace@yahoo.com, rsmediros@cbn.net.id', '3172', '3172090', '3172090007', '-6.179724', '106.903748', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(79, 84, 'Premier Jatinegara', 'Rumah Sakit Umum', 'Jl. Raya Jatinegara Timur No. 85 - 87', '-6.221951', '106.868935', '13310', '2800666|2800777|2800888', '2800755', 'www.ramsayhealth.co.id', 'rspremier.jatinegara@ramsayhealth.co.id', '3172', '3172060', '3172060007', '-6.221951', '106.868935', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(80, 85, 'Pengayoman Cipinang', 'Rumah Sakit Umum', 'Jl. Raya Bekasi Timur 170 B', '-6.221951', '106.868935', '13410', '085210432196|85914558', '85909644', NULL, 'rspengayoman.dki@gmail.com', '3172', '3172060', '3172060007', '-6.221951', '106.868935', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(81, 86, 'Yadika Pondok Bambu', 'Rumah Sakit Umum', 'Jl. Pahlawan Revolusi No. 47', '-6.237237', '106.898796', '13430', '8615754-8610756', '8631708', NULL, 'rsyadika@yahoo.co.id', '3172', '3172070', '3172070001', '-6.237237', '106.898796', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(82, 87, 'Islam Jakarta Pondok Kopi', 'Rumah Sakit Umum', 'Jl. Raya Pondok Kopi', '-6.220089', '106.939682', '13460', '8610471 |8630654', '8611101', 'www.rsijpondokkopi.co.id', 'aliyus_k@yahoo.com, ariefb12@gmail.com', '3172', '3172070', '3172070004', '-6.220089', '106.939682', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(83, 88, 'Dik Pusdikkes Kodiklat TNI AD', 'Rumah Sakit Umum', 'Jl. Raya Bogor', '-6.275258', '106.869537', '13510', '8092358|8003491', '8092706|80887304', 'www.pusdikkes.com', 'rs.dik.pusdikkes@gmail.com', '3172', '3172050', '3172050005', '-6.275258', '106.869537', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(84, 89, 'Bhayangkara Tk.I  R. Said Sukanto', 'Rumah Sakit Umum', 'Jl. RS Polri', '-6.269806', '106.870674', '13510', '8093288', '8094005', NULL, 'rs_polri@pdpersi.co.id', '3172', '3172050', '3172050005', '-6.269806', '106.870674', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(85, 90, 'Al-Fauzan', 'Rumah Sakit Umum', 'Jalan  Pedati Nomor 3  RT.005 / RW.07', '-6.297779', '106.870377', '13540', '8402821', '87710318 ', 'www.jih.co.id', 'jakartaislamichospital@yahoo.com', '3172', '3172050', '3172050003', '-6.297779', '106.870377', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(86, 91, 'Haji Jakarta', 'Rumah Sakit Umum', 'Jl. Raya Pondok Gede No. 4', '-6.290169', '106.888847', '13560', '8000693-695', '80876209|8000702', NULL, 'keliek_marwanto@yahoo.co.id, rshaji@pdpersi.co.id', '3172', '3172040', '3172040001', '-6.290169', '106.888847', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(87, 92, 'Pusat Angkatan Udara Dr. Esnawan Antariksa', 'Rumah Sakit Umum', 'Jl. Merpati No. 2,  Komplek Rajawali', '-6.257015', '106.891922', '13610', '8091716-8093943', '8098665', NULL, 'ruspau@pdpersi.co.id', '3172', '3172040', '3172040004', '-6.257015', '106.891922', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(88, 93, 'Harum Sisma Medika', 'Rumah Sakit Umum', 'Jl. Inspeksi Tarum Barat - Kalimalang', '-6.24808', '106.90963', '13620', '8617212-13', '8601030', NULL, 'erwintyrana@sismamedika.com, rsharum@pdpersi.co.id', '3172', '3172040', '3172040005', '-6.24808', '106.90963', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(89, 94, 'Budhi Asih ', 'Rumah Sakit Umum Daerah', 'Jl. Dewi Sartika  Cawang III  No. 200', '-6.256015', '106.862869', '13630', '8090282|8092482', '8009157-8007348', 'www.rsudbudhiasih.com', 'budhiasih@ymail.com', '3172', '3172050', '3172050007', '-6.256015', '106.862869', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(90, 95, 'Universitas Kristen Indonesia', 'Rumah Sakit Umum', 'Jl. Mayjen Sutoyo No. 2', '-6.25092', '106.872002', '13630', '8092317|8092831|8010523', '8092445', NULL, 'rsuuki@yahoo.co.id', '3172', '3172050', '3172050007', '-6.25092', '106.872002', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(91, 96, 'Olahraga Nasional', 'Rumah Sakit Umum', 'Jalan  Jambore  Nomor 1', '-6.368448', '106.892929', '13720', '87753977', '021-87753977', NULL, 'rsolahragacibubur@gmail.com', '3172', '3172020', '3172020001', '-6.368448', '106.892929', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(92, 97, 'Tingkat IV Cijantung Kesdam Jaya', 'Rumah Sakit Umum', 'Jl. Mahoni,  Cijantung II ', '-6.309916', '106.859673', '13760', '8400535', '8407886', NULL, 'kesdamjaya_cijantung@yahoo.com', '3172', '3172010', '3172010005', '-6.309916', '106.859673', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(93, 98, 'Pasar Rebo', 'Rumah Sakit Umum Daerah', 'Jl. Letjen T. B. Simatupang No. 30', '-6.304108', '106.861931', '13760', '8401127/ 8400109', '8411159', 'www.rsudpasarrebo.com', 'rsudpasarrebo@yahoo.com', '3172', '3172010', '3172010005', '-6.304108', '106.861931', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(94, 99, 'Adhyaksa', 'Rumah Sakit Umum', 'Jl. Hankam Raya No.60', '-6.304108', '106.861931', '13820', '021-29462345', '', NULL, NULL, '3172', '3172010', '3172010005', '-6.304108', '106.861931', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(95, 100, 'Harapan Bunda', 'Rumah Sakit Umum', 'Jl. Raya Bogor KM. 22 No. 44', '-6.29439', '106.880798', '13830', '8400257', '87781247', NULL, 'hrd@rsharapanbunda.com, harapanbunda@pdpersi.co.id', '3172', '3172050', '3172050004', '-6.29439', '106.880798', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(96, 101, 'Harapan Jayakarta ', 'Rumah Sakit Umum', 'Jalan  Bekasi Timur Raya  Nomor 6  KM.18', '-6.195012', '106.906258', '13930', '4608886|4603916|46821726', '4608863', NULL, NULL, '3172', '3172080', '3172080001', '-6.195012', '106.906258', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(97, 102, 'Kepulauan Seribu', 'Rumah Sakit Umum Daerah', 'Pulau Pramuka', '-5.744879', '106.613396', '14510', '021-33230332|33312388|36571981', '33312388|37175900', NULL, 'rsukepulauanseribu@yahoo.co.id', '3101', '3101020', '3101020001', '-5.744879', '106.613396', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(98, 103, 'Cempaka Putih', 'Rumah Sakit Umum Kecamatan', 'Jalan  Rawasari Selatan  Nomor 1', '-6.184666', '106.869911', '10510', '4219548|42801341', '4219548|42801341', NULL, NULL, '3173', '3173050', '3173050002', '-6.184666', '106.869911', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(99, 104, 'Sawah Besar', 'Rumah Sakit Umum Kecamatan', 'Jalan  Dwiwarna Raya  Nomor 6-8', '-6.149973', '106.829605', '10740', '6289080|4224041', '6012240', NULL, NULL, '3173', '3173070', '3173070004', '-6.149973', '106.829605', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(100, 105, 'Johar Baru', 'Rumah Sakit Umum Kecamatan', 'Jalan  Tanah Tinggi XII', '-6.181071', '106.849373', '10540', ' 4246359', '4224041', NULL, NULL, '3173', '3173040', '3173040003', '-6.181071', '106.849373', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(101, 106, 'Kemayoran', 'Rumah Sakit Umum Kecamatan', 'Jalan  Serdang Baru I', '-6.1608', '106.862289', '10650', '4801847', '4801846', NULL, 'rsukemayoran@gmail.com', '3173', '3173060', '3173060004', '-6.1608', '106.862289', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(102, 107, 'Koja', 'Rumah Sakit Umum Kecamatan', 'Jalan  Walang Permai  Nomor 39', '-6.127233', '106.906982', '14260', '43905651|4358809', '', NULL, NULL, '3175', '3175040', '3175040003', '-6.127233', '106.906982', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(103, 108, 'Cilincing', 'Rumah Sakit Umum Kecamatan', 'Jalan  Madya Kebantenan  Nomor 4', '-6.118146', '106.927902', '14130', '4412889|4416367', '4412889|4416367', NULL, NULL, '3175', '3175060', '3175060005', '-6.118146', '106.927902', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(104, 109, 'Pademangan', 'Rumah Sakit Umum Kecamatan', 'Jalan  Budi Mulia Raya  Nomor 2  RT.0015 / RW.011', '-6.133254', '106.839401', '14420', '021-6452346', '', NULL, NULL, '3175', '3175020', '3175020001', '-6.133254', '106.839401', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(105, 110, 'Kembangan', 'Rumah Sakit Umum Kecamatan', 'Jalan  Topaz Blok F2  Nomor 3', '-6.181012', '106.733223', '11620', '5870834', '', NULL, NULL, '3174', '3174010', '3174010006', '-6.181012', '106.733223', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(106, 111, 'Kalideres', 'Rumah Sakit Umum Kecamatan', 'Jalan  Satu Maret  Nomor 48  RT.001 / RW.04', '-6.128841', '106.704369', '11830', '54390576', '54390576', NULL, 'rsukalideres@gmail.com', '3174', '3174080', '3174080003', '-6.128841', '106.704369', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(107, 112, 'Jagakarsa', 'Rumah Sakit Umum Kecamatan', 'Jalan  Mohamad Kahfi 1  Nomor 27 A', '-6.311328', '106.813515', '12620', '7864707', '', NULL, NULL, '3171', '3171010', '3171010004', '-6.311328', '106.813515', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(108, 113, 'Tebet', 'Rumah Sakit Umum Kecamatan', 'Jalan  Prof. Supomo, SH  Nomor 54', '-6.231592', '106.846008', '12810', '8314955', '8296918', NULL, NULL, '3171', '3171090', '3171090002', '-6.231592', '106.846008', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(109, 114, 'Mampang Prapatan', 'Rumah Sakit Umum Kecamatan', 'Jalan  Kapten Tendean  Nomor 9', '-6.240428', '106.826988', '12790', '79192187', '', NULL, NULL, '3171', '3171070', '3171070004', '-6.240428', '106.826988', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(110, 115, 'Pesanggrahan', 'Rumah Sakit Umum Kecamatan', 'Jalan  Cenek I  Nomor 1', '-6.258924', '106.75766', '12320', '7356087', '', NULL, NULL, '3171', '3171040', '3171040002', '-6.258924', '106.75766', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(111, 116, 'Kramat Jati', 'Rumah Sakit Umum Kecamatan', 'Jalan  Raya Inpres  Nomor 48', '-6.28674', '106.862251', '13540', '87791152|87793604', ' 87793604', NULL, NULL, '3172', '3172050', '3172050003', '-6.28674', '106.862251', '2021-01-26 06:25:52', '2021-01-26 06:25:52'),
(112, 117, 'Ciracas', 'Rumah Sakit Umum Kecamatan', 'Jalan  Raya Lapangan Tembak', '-6.25801', '106.905167', '13720', '8718995', '', NULL, NULL, '3172', '3172040', '3172040004', '-6.25801', '106.905167', '2021-01-26 06:25:52', '2021-01-26 06:25:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  `organization_id` varchar(10) DEFAULT NULL,
  `organization_name` varchar(100) DEFAULT NULL,
  `hub_id` varchar(50) DEFAULT NULL,
  `hub_name` varchar(150) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  `role` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `status`, `organization_id`, `organization_name`, `hub_id`, `hub_name`, `email`, `name`, `phone`, `password`, `remember_token`, `role`, `created_at`, `updated_at`) VALUES
(1, NULL, '1', 'PT Testing', '1', 'Hub Jakarta', 'radzi@jsc.com', 'radzi-jsc', '08999032337', '$2y$10$rqmsmU.7mNeRPaAYugOUIeFiZy5i9bwpNURfmeFdCe7Wh0240XXJG', 'WcsMyshROJXW9CW2evFDrYc66EF5nQ1YOySPXnW6PM9GUy3T2TtZsJrGH5cK', 'vehicle-list | vehicle-add | vehicle-edit | hub-list | hub-add | hub-edit | zone-list | zone-add | zone-edit | address-list | address-add | address-edit | user-list | user-add | client-list | client-add | client-edit | job-list | job-import | job-assign | job-unassign | map-list | org-list | summary-list | manualpod-list', '2019-04-23 06:49:30', '2020-01-16 09:54:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_aliases`
--
ALTER TABLE `file_aliases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelurahans`
--
ALTER TABLE `kelurahans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rumah_sakits`
--
ALTER TABLE `rumah_sakits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_aliases`
--
ALTER TABLE `file_aliases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kelurahans`
--
ALTER TABLE `kelurahans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rumah_sakits`
--
ALTER TABLE `rumah_sakits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

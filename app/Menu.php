<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['dashboard','zone','hub','vehicle','job','manual_pod','failed_reason','organization_setting'];   
}
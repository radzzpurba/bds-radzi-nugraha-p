<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RumahSakit extends Model
{
    protected $fillable = [
    	'id_rsu',
    	'nama_rsu',
    	'jenis_rsu',
    	'alamat',
    	'latitude_object',
    	'longitude_object',
    	'kode_pos',
    	'telepon',
    	'faximile',
    	'website',
    	'email',
    	'kode_kota',
    	'kode_kecamatan',
    	'kode_kelurahan',
    	'latitude',
    	'longitude'
    ];   

    public function kelurahan()
    {
        return $this->hasOne('App\Kelurahan','kode_kelurahan','kode_kelurahan');
    }
}
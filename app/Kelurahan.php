<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $fillable = [
    	'kode_provinsi',
    	'nama_provinsi',
    	'kode_kota',
    	'nama_kota',
    	'kode_kecamatan',
    	'nama_kecamatan',
    	'kode_kelurahan',
    	'nama_kelurahan',
    ];   
}
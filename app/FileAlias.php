<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileAlias extends Model
{
   protected $fillable = [
    'external_id', // => id dari client
    'job_date',
    'job_number', // => order_no
    'reference_no',
    'order_type',
    'job_type',
    'hub_id', // group_id
    'hub_name', // group_name
    'payment_amount', //amount
    'payment_type',
    'deliver_to', //addresse
    'edit_price',
    'relation_recipient',
    'note',
    'instruction',
    'collected_amount',


    'latitude',
    'longitude',
    'address',
    'address_2',
    'province',
    'city',
    'district',
    'subdistrict',
    'postalcode',
    'country',
    'phone',
    'notify_email', //email
    'order_note',

    'province_alias',
    'city_alias',
    'district_alias',
    'subdistrict_alias',

   


    'internal_id', // => id   
    'organization_id',
    'assign_to',
    'vehicle_name',
    'organization_name',
    'status',
    'sheet_id',
    'zone_id',
    'zone_name',

    'item_sku',
    'item_qty',
    'item_description',
    'unit_price',



    'free_text_1','free_text_2','free_text_3','free_text_4','free_text_5','free_text_6','free_text_7','free_text_8','free_text_9','free_text_10'
    ];   
}


<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Menu;
use App\FileAlias;
use App\Organization;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

       

        view()->composer('*', function ($view) {
           
            if(isset(Auth::user()->organization_id)){
                $org_id = Auth::user()->organization_id;
                //$org = Organization::select(['id','timezone','logo'])->where('id',$timezone)->first();
                //$orgTime = $org['timezone'];
                //$orgLogo = $org['logo'];

                $menu = Menu::where('organization_id',$org_id)->first();
                $fields = FileAlias::where('organization_id',$org_id)->first();
                $org = Organization::where('id',$org_id)->first();
            }
            else{
                //$orgTime = 7;
            }
            
            $view->with(compact('menu','fields','org'));
            
        });
    }
}

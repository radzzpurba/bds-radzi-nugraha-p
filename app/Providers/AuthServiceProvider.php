<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Vehicle' => 'App\Policies\VehiclePolicy',
        'App\Zone' => 'App\Policies\ZonePolicy',
        'App\Hub' => 'App\Policies\HubPolicy',
        'App\Address' => 'App\Policies\AddressPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Client' => 'App\Policies\ClientPolicy',
        'App\Job' => 'App\Policies\JobPolicy',
        'App\Other' => 'App\Policies\OtherPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /* Vehicle */
        Gate::define('vehicle-list', 'App\Policies\VehiclePolicy@vehicleList');
        Gate::define('vehicle-add', 'App\Policies\VehiclePolicy@vehicleAdd');
        Gate::define('vehicle-edit', 'App\Policies\VehiclePolicy@vehicleEdit');

        /* Zone */
        Gate::define('zone-list', 'App\Policies\ZonePolicy@zoneList');
        Gate::define('zone-add', 'App\Policies\ZonePolicy@zoneAdd');
        Gate::define('zone-edit', 'App\Policies\ZonePolicy@zoneEdit');

        /* Hub */
        Gate::define('hub-list', 'App\Policies\HubPolicy@hubList');
        Gate::define('hub-add', 'App\Policies\HubPolicy@hubAdd');
        Gate::define('hub-edit', 'App\Policies\HubPolicy@hubEdit');

        /* User */
        Gate::define('user-list', 'App\Policies\UserPolicy@userList');
        Gate::define('user-add', 'App\Policies\UserPolicy@userAdd');
        Gate::define('user-edit', 'App\Policies\UserPolicy@userEdit');

        /* Job */
        Gate::define('job-list', 'App\Policies\JobPolicy@jobList');
        Gate::define('job-import', 'App\Policies\JobPolicy@jobImport');
        Gate::define('job-unassign', 'App\Policies\JobPolicy@jobUnassign');
        Gate::define('job-assign', 'App\Policies\JobPolicy@jobassign');

         /* Address */
        Gate::define('address-list', 'App\Policies\AddressPolicy@addressList');
        Gate::define('address-add', 'App\Policies\AddressPolicy@addressAdd');
        Gate::define('address-edit', 'App\Policies\AddressPolicy@addressEdit');

        /* Client */
        Gate::define('client-list', 'App\Policies\ClientPolicy@clientList');
        Gate::define('client-add', 'App\Policies\ClientPolicy@clientAdd');
        Gate::define('client-edit', 'App\Policies\ClientPolicy@clientEdit');

        /* Other */
        Gate::define('manualpod-list', 'App\Policies\OtherPolicy@manualpodList');
        Gate::define('summary-list', 'App\Policies\OtherPolicy@summaryList');
        Gate::define('failedreason-list', 'App\Policies\OtherPolicy@failedReasonList');
        Gate::define('org-list', 'App\Policies\OtherPolicy@orgList');
        Gate::define('map-list', 'App\Policies\OtherPolicy@mapList');

        




        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['generated_id','name','success_photo','success_sign','failed_photo','failed_sign','phone_code','latitude','longitude','zoom','interval','image','favicon','sm_url','sm_api_key'];   
}


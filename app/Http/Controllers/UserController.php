<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Hub;
use App\Invoice;
use App\Helpers\UserHelper;
use App\Helpers\InvoiceHelper;
use App\Helpers\EmailUserHelper;
use DataTables;
use Validator;
use Illuminate\Validation\Rule;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Support\Facades\Hash;
use DB;
use Gate;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){


        $hubs = Hub::where('organization_id',$request->user()->organization_id)->where('is_active','active')->orderBy('name')->get();


        $titlePage="MEMBER MANAGEMENT";
        $pageName="Member";

        if ($request->ajax()) {
            $users=DB::table('users')->orderBy('created_at','desc');
            //if($request->input('organization')!='') $seller=$seller->where('organization_id',$request->input('organization'));
            //if($request->input('client')!='') $seller=$seller->where('client_id',$request->input('client_id'));
            
            return datatables()->query($users)
                ->addColumn('actions', function($usersAction) {

                if(Gate::denies('user-add')) $button = "-";
                else{
                    $button= '<a data-id="'.$usersAction->id.'|||'.$usersAction->name.'|||'.$usersAction->phone.'|||'.$usersAction->email.'|||'.$usersAction->status.'|||'.$usersAction->role.'|||'.$usersAction->hub_id.'" class="btn btn-outline-info btn-sm buttonEdit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>';
                } 

                return $button;
            })
            ->rawColumns(['link', 'actions'])
            ->toJson();

        }

        return view('backend.user.index',compact('titlePage','pageName','hubs'));
    }

    public function add(Request $request){
        if ($request->ajax()) {

            $params = ['request'=>$request,'type'=>'add'];

            $validator = $this->validation($params);

            if ($validator['status']=='success') {
                $userHelper = new UserHelper();
                $addUser = $userHelper->add($request);

                return response()->json(['status'=>$validator['status'],'message'=>'Success']);
            }

            return response()->json(['status'=>$validator['status'],'message'=>$validator['message']]);
        }
    }

    

    public function edit(Request $request){
        if ($request->ajax()) {
            $params = ['request'=>$request,'type'=>'edit'];
            $validator = $this->validation($params);

            if ($validator['status']=='success') {

                $userHelper = new UserHelper();
                $addUser = $userHelper->edit($request);

                return response()->json(['status'=>$validator['status'],'message'=>'Success']);
            }

        }   

        return response()->json(['status'=>$validator['status'],'message'=>$validator['message']]);
    }



    public function validation($params){
        if($params['type']=='add'){
            $validate = Validator::make($params['request']->all(), [
                'name' => 'required|unique:users', 
                'email' => 'required|unique:users', 
                'password' => 'required', 
            ]);
        }
        
        elseif($params['type']=='edit'){
            $validate = Validator::make($params['request']->all(), [
                'name' =>[
                        'required',
                        //Rule::unique('users')->ignore($params['request']['id'], 'generated_id')
                ],
                'email' =>[
                        'required',
                        Rule::unique('users')->ignore($params['request']['id'], 'id')
                ],
                
            ]);
        }

        if ($validate->passes()) $status = 'success';
        else $status = 'error';
        
        $data = ['status'=>$status,'message'=>$validate->getMessageBag()->toArray()];
        return $data;
    }

}

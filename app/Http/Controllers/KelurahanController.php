<?php

namespace App\Http\Controllers;

    /* Backend UI Library*/
        use Illuminate\Http\Request;
        use App\Kelurahan;
        use DataTables;
        use DB;
    /* END Backend UI */


    /* API Library */
        use GuzzleHttp\Exception\GuzzleException;
        use GuzzleHttp\Client;
        use GuzzleHttp\Psr7\Response;
    /* END API */


class KelurahanController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $pageName = "kelurahan";

        if ($request->ajax()) {
           
            $data = DB::table('kelurahans')->orderBy('created_at');
            return datatables()->query($data)
                ->addColumn('created', function ($record){
                    $date = date('d M Y H:i:s', strtotime($record->created_at. '+7 hours'));

                   return $date;
                })
                ->addColumn('updated', function ($record){
                    $date = date('d M Y H:i:s', strtotime($record->updated_at. '+7 hours'));

                   return $date;
                })
                ->toJson();

        }

        return view('backend.kelurahan.index',compact('pageName'));
    }

    public function getKelurahanAPI(Request $request){
        $client = new Client([
            'base_uri' => "http://api.jakarta.go.id"
        ]);

        $headers = [
            'Authorization' => 'LdT23Q9rv8g9bVf8v/fQYsyIcuD14svaYL6Bi8f9uGhLBVlHA3ybTFjjqe+cQO8k',
            'Content-Type'=>'application/json',
            'Accept'=>'application/json'
        ];

        $hit =  $client->get('/v1/kelurahan', ['headers'=>$headers]);

        $return = json_decode($hit->getBody());

        foreach($return->data as $data){
           $checkExisting = Kelurahan::where('kode_kelurahan',$data->kode_kelurahan)->first();

            //insert new data
            if(!isset($checkExisting)){
                $insert = new Kelurahan();
                $insert->kode_provinsi = $data->kode_provinsi;
                $insert->nama_provinsi = $data->nama_provinsi;
                $insert->kode_kota = $data->kode_kota;
                $insert->nama_kota = $data->nama_kota;
                $insert->kode_kecamatan = $data->kode_kecamatan;
                $insert->nama_kecamatan = $data->nama_kecamatan;
                $insert->kode_kelurahan = $data->kode_kelurahan;
                $insert->nama_kelurahan = $data->nama_kelurahan;
                $insert->save();
            }
            else{
                //update existing data
                $rs_record = Kelurahan::where('kode_kelurahan',$data->kode_kelurahan)->update([
                    'kode_provinsi' => $data->kode_provinsi,
                    'nama_provinsi' => $data->nama_provinsi,
                    'kode_kota' => $data->kode_kota,
                    'nama_kota' => $data->nama_kota,
                    'kode_kecamatan' => $data->kode_kecamatan,
                    'nama_kecamatan' => $data->nama_kecamatan,
                    'kode_kelurahan' => $data->kode_kelurahan,
                    'nama_kelurahan' => $data->nama_kelurahan
                ]);
            }
           
        }

        return view('backend.kelurahan.redirect');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Image;
use App\User;
use App\Invoice;

class FrontEndController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $totalMatch = Event::whereNotIn('id',[0])->count();
        $totalMatch = $totalMatch+14;
        $today=date('Y-m-d');
        $totalEvents=0;
        $events = Event::where('event_date','>=',$today)->orderBy('event_date','asc')->get()->take(3);
        if(isset($events))$totalEvents=count($events);
        $link = Event::where('event_date','<',$today)->orderBy('event_date','desc')->get()->take(3);

        $image_one=Image::where('location','landing_page_one')->first();
        $image_two=Image::where('location','landing_page_two')->first();
        $image_three=Image::where('location','landing_page_three')->first();
        
        if(isset($request->user()->generated_id)){
            $findPoint=User::select(['generated_id','point'])->where('generated_id',$request->user()->generated_id)->first()->point;
            $countRequested=Invoice::select(['status','event_date'])->where('status','requested')->where('event_date','>=',date('Y-m-d'));
            if($request->user()->role!="admin")$countRequested=$countRequested->where('requested_by',$request->user()->generated_id);
            $countRequested = $countRequested->count();
        }
        return view('frontend.index',compact('events','link','totalEvents','image_one','image_two','image_three','totalMatch','findPoint','countRequested'));
    }

    public function faq(Request $request)
    {
        if(isset($request->user()->generated_id)){
            $findPoint=User::select(['generated_id','point'])->where('generated_id',$request->user()->generated_id)->first()->point;
            $countRequested=Invoice::select(['status','event_date'])->where('status','requested')->where('event_date','>=',date('Y-m-d'));
            if($request->user()->role!="admin")$countRequested=$countRequested->where('requested_by',$request->user()->generated_id);
            $countRequested = $countRequested->count();
        }

        return view('frontend.faq',compact('findPoint','countRequested'));
    }

    public function rules_point(Request $request)
    {
         if(isset($request->user()->generated_id)){
            $findPoint=User::select(['generated_id','point'])->where('generated_id',$request->user()->generated_id)->first()->point;
            $countRequested=Invoice::select(['status','event_date'])->where('status','requested')->where('event_date','>=',date('Y-m-d'));
            if($request->user()->role!="admin")$countRequested=$countRequested->where('requested_by',$request->user()->generated_id);
            $countRequested = $countRequested->count();
        }

        $image=Image::where('location','tukar_point')->first();
        return view('frontend.rules_point',compact('image','findPoint','countRequested'));
    }
}

<?php

namespace App\Http\Controllers;

    /* Backend UI Library*/
        use Illuminate\Http\Request;
        use App\RumahSakit;
        use DataTables;
        use DB;
    /* END Backend UI */


    /* API Library */
        use GuzzleHttp\Exception\GuzzleException;
        use GuzzleHttp\Client;
        use GuzzleHttp\Psr7\Response;
    /* END API */


class RumahSakitController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $pageName = "rumah_sakit";

        if ($request->ajax()) {
           
            $data = DB::table('rumah_sakits')->orderBy('created_at');
            return datatables()->query($data)
                ->addColumn('created', function ($record){
                    $date = date('d M Y H:i:s', strtotime($record->created_at. '+7 hours'));

                   return $date;
                })
                ->addColumn('updated', function ($record){
                    $date = date('d M Y H:i:s', strtotime($record->updated_at. '+7 hours'));

                   return $date;
                })
                ->toJson();

        }

        return view('backend.rumah_sakit.index',compact('pageName'));
    }

    public function getRumahSakitAPI(Request $request){
        $client = new Client([
            'base_uri' => "http://api.jakarta.go.id"
        ]);

        $headers = [
            'Authorization' => 'LdT23Q9rv8g9bVf8v/fQYsyIcuD14svaYL6Bi8f9uGhLBVlHA3ybTFjjqe+cQO8k',
            'Content-Type'=>'application/json',
            'Accept'=>'application/json'
        ];

        $hit =  $client->get('/v1/rumahsakitumum', ['headers'=>$headers]);

        $return = json_decode($hit->getBody());

        foreach($return->data as $data){
            $telepon = $this->implodeArray($data->telepon);
            $faximile = $this->implodeArray($data->faximile);

            $checkExisting = RumahSakit::where('id_rsu',$data->id)->first();

            //insert new data
            if(!isset($checkExisting)){
                $insert = new RumahSakit();
                $insert->id_rsu = $data->id;
                $insert->nama_rsu = $data->nama_rsu;
                $insert->jenis_rsu = $data->jenis_rsu;
                $insert->alamat = $data->location->alamat;
                $insert->latitude_object = $data->location->latitude;
                $insert->longitude_object = $data->location->longitude;
                $insert->kode_pos = $data->kode_pos;
                $insert->telepon = $telepon;
                $insert->faximile = $faximile;
                $insert->website = $data->website;
                $insert->email = $data->email;
                $insert->kode_kota = $data->kode_kota;
                $insert->kode_kecamatan = $data->kode_kecamatan;
                $insert->kode_kelurahan = $data->kode_kelurahan;
                $insert->latitude = $data->latitude;
                $insert->longitude = $data->longitude;
                $insert->save();
            }
            else{
                //update existing data
                $rs_record = RumahSakit::where('id_rsu',$data->id)->update([
                    'nama_rsu' => $data->nama_rsu,
                    'jenis_rsu' => $data->jenis_rsu,
                    'alamat' => $data->location->alamat,
                    'latitude_object' => $data->location->latitude,
                    'longitude_object' => $data->location->longitude,
                    'kode_pos' => $data->kode_pos,
                    'telepon' => $telepon,
                    'faximile' => $faximile,
                    'website' => $data->website,
                    'email' => $data->email,
                    'kode_kota' => $data->kode_kota,
                    'kode_kecamatan' => $data->kode_kecamatan,
                    'kode_kelurahan' => $data->kode_kelurahan,
                    'latitude' => $data->latitude,
                    'longitude' => $data->longitude
                ]);
            }
           
        }

        return view('backend.rumah_sakit.redirect');
    }


    private function implodeArray($string){
        return $data = implode('|', $string);
    }

}

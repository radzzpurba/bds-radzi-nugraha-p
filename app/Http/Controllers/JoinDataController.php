<?php

namespace App\Http\Controllers;

    
use Illuminate\Http\Request;
use App\RumahSakit;

    /* API Library */
        use GuzzleHttp\Exception\GuzzleException;
        use GuzzleHttp\Client;
        use GuzzleHttp\Psr7\Response;
    /* END API */


class JoinDataController extends Controller
{



    public function index(Request $request){
        $status = "success";
        $rumaSakit = RumahSakit::with('kelurahan')->get()->take(5);
        $count = count($rumaSakit);

        
        $data=[];
        $new_data=[];
        foreach($rumaSakit as $rs){
            $telepon= $this->explodeString($rs['telepon']);
            $faximile= $this->explodeString($rs['faximile']);

            $new_data['id']=$rs['id_rsu'];
            $new_data['nama_rsu']=$rs['nama_rsu'];
            $new_data['jenis_rsu']=$rs['jenis_rsu'];
            $new_data['location']=["latitude"=>$rs['latitude_object'],"longitude"=>$rs['longitude']];
            $new_data['alamat']=$rs['alamat'];
            $new_data['kode_pos']=$rs['kode_pos'];
            $new_data['telepon']=$telepon;
            $new_data['faximile']=$faximile;
            $new_data['website']=$rs['website'];
            $new_data['email']=$rs['email'];
            $new_data['kelurahan']=[ 
                "kode"=>$rs['kode_kelurahan'],
                "nama"=>$rs['kelurahan']['nama_kelurahan']
            ];
            $new_data['kecamatan']=[
                "kode"=>$rs['kode_kecamatan'],
                "nama"=>$rs['kelurahan']['nama_kecamatan']
            ];
            $new_data['kota']=[
                "kode"=>$rs['kode_kota'],
                "nama"=>$rs['kelurahan']['nama_kota']
            ];

            array_push($data,$new_data);
        }

        return response()->json([
            'status'=>$status,
            'count'=>$count,
            'data'=>$data
        ], 200);
    }

    private function explodeString($string){
        return $array = explode("|", $string);
    }


}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//dashboard
Route::get('/', 'KelurahanController@index')->name('kelurahan_list');

//Rumah Sakit
Route::get('/rumah-sakit/list', 'RumahSakitController@index')->name('rs_list');
Route::get('/rumah-sakit/getRumahSakitAPI', 'RumahSakitController@getRumahSakitAPI')->name('rs_getRumahSakitAPI');

//Kelurahan
Route::get('/kelurahan/list', 'KelurahanController@index')->name('kelurahan_list');
Route::get('/kelurahan/getKelurahanAPI', 'KelurahanController@getKelurahanAPI')->name('kelurahan_getKelurahanAPI');

//API view
Route::get('/join-data-rs-kelurahan', 'JoinDataController@index')->name('data_list');



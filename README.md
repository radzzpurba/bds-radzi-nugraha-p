# JSC Testing

1. URL untuk Testing System secara Live

http://jsc.akusistem.com/public/login

username: radzi@jsc.com

password: radzitms

2. endpoint API Live
Endpoint live untuk melihat hasil joint antara Rumah Sakit dan Kelurahan

http://jsc.akusistem.com/public/api/join-data-rs-kelurahan 

3. endpoint API Local
Endpoint local untuk melihat hasil joint antara Rumah Sakit dan Kelurahan

http://localhost/jsc_git/public/api/join-data-rs-kelurahan

4. installation

Test bisa dilakukasn secara live melalui link yg saya sertakan diatas, 
namun jika ingin melakukan penginstallan pada perangkat Anda, berikut ini adalah langkah-langkahnya: 

a. clone repo ini, kedalam folder XAMPP/htdocs

(pastikan perangkat sudah ter-install XAMPP dan PHP My Admin)

b. import database

database yang digunakan adalah MySQL

import database yang sudah disediakan (file dengan nama jsc_database.sql)


credentials:

nama database: jsc_database

username database: root

password username: 


c. buka aplikasi melalui browser

localhost/nama_folder/public/login


anda akan melihat halaman login 


login dengan credentials:


username: radzi@jsc.com

password: radzitms
